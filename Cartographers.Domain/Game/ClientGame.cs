﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.Domain.ResearchCardData;
using Cartographers.Domain.Surfaces;
using Cartographers.DTO;
using System.Linq;

namespace Cartographers.Domain.Game
{
	public class ClientGame : Game
	{

		public ClientGame(ResearchCardFactory rcf, DecreeFactory df) : this(CreateDefaultGameDto(), rcf, df)
		{ }

		public ClientGame(GameDTO gameDto, ResearchCardFactory rcf, DecreeFactory df): base(gameDto, rcf, df)
		{
			var newSurroundedMountains = Map.GetFieldsOnMapWithNeighbor()
											.Where(w => w.Field == FieldType.Mountain &&
																	w.Neighbors.All(a => a.Field != FieldType.Empty &&
																						a.Field != FieldType.None &&
																						a.Field != FieldType.Ruin))
											.Select(s => (Point)(s.CoordI, s.CoordJ));
			_surroundedMountains.AddRange(newSurroundedMountains);
		}

		public override void Move(Point position)
		{
			base.Move(position);
			Figure = new Figure(new FieldType[0, 0]);
		}

		private static GameDTO CreateDefaultGameDto()
		{
			var board = new BoardDTO
			{
				Width = Map.DefaultMapSize,
				Height = Map.DefaultMapSize,
				Fields = new FieldType[Map.DefaultMapSize * Map.DefaultMapSize]
			};
			return new GameDTO
			{
				Board = board,
				DecreeIds = new int[4] {1, 1, 1, 1},
				ResearchCardId = 1,
				Score = 0,
				SeasonId = 0,
				SeasonTurn = -1
			};
		}
	}
}
