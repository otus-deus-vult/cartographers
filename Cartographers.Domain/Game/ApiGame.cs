﻿using System.Linq;
using Cartographers.DTO;
using Cartographers.Domain.ResearchCardData;
using Cartographers.Domain.SeasonData;
using Cartographers.Domain.Surfaces;
using System;
using Cartographers.Domain.DecreeData;

namespace Cartographers.Domain.Game
{
	public class ApiGame: Game
	{
		private readonly ResearchCardFactory _rcf;
		public GamerScore GamerScore { get; }

		public ApiGame(GameDTO gameDto, ResearchCardFactory rcf, DecreeFactory df) : base(gameDto, rcf, df)
		{
			_rcf = rcf;
			GamerScore = new GamerScore();
		}

		public GameDTO GetGameDTO()
		{
			return new GameDTO
			{
				Board = Map.GetDTO(),
				DecreeIds = Decrees.Select(d => (int)d.Code).ToArray(),
				ResearchCardId = (int)Card.Code,
				Score = Score,
				SeasonId = (int)Season.Code,
				SeasonTurn = SeasonTurn,
				TurnAvailable = TurnAvailable,
				TurnTimeLeft = TurnTimeLeft,
				GameCancelled = GameCancelled,
				Coins = Coins,
				Monsters = Monsters,
				IsRuinCard = Card.IsRuin
			};
		}

		public bool Move(MoveDTO move)
		{
			CurrentFieldIndex = move.Changes.FieldIndex;
			CurrentShapeIndex = move.Changes.ShapeIndex;
			TakeFigureFromCard();
			if (move.Changes.IsReflectedHorizontal) 
				ReflectFigure();
			if (move.Changes.IsReflectedVertical)
				ReflectFigure(true);
			if (move.Changes.RotateCount >= 4) move.Changes.RotateCount %= 4;
			for (int i = 0; i < move.Changes.RotateCount; ++i)
				RotateFigure();
			if (!CanMove(move.Position))
				return false;
			Move(move.Position);
			return true;
		}

		public void NextTurn(Season season, int seasonTurn, IResearchCard card, DateTime turnEndTime)
		{
			Season = season;
			SeasonTurn = seasonTurn;
			Card = card;
			TurnTimeLeft = (int)(turnEndTime - DateTime.Now).TotalSeconds;
			if (!HaveAnyMove())
				Card = _rcf.GetResearchCardByCode(ResearchCode.Anomaly);
			TurnAvailable = true;
		}

		internal bool HaveAnyMove()
		{
			foreach (var shape in Card.Shapes)
			{
				var figure = new Figure(shape, FieldType.Farm);
				for (int i = 0; i < 2; ++i)
				{
					for (int j = 0; j < 4; ++j)
					{
						for (int h = 0; h < Map.Height; ++h)
						{
							for (int w = 0; w < Map.Width; ++w)
								if (Map.CanAdd(figure, new Point(w, h), Card.IsRuin))
									return true;
						}
						figure.Rotate();
					}
					figure.ReflectHorizontal();
				}
			}

			return false;
		}

		public void CountScore()
		{
			int season = (int)Season.Code - (GameHasEnded ? 0 : 1);
			GamerScore.Monsters[season] = Monsters;
			GamerScore.DecreeScore[season * 2] = Decrees[season].CountScore(Map);
			GamerScore.DecreeScore[season * 2 + 1] = Decrees[(season + 1) % 4].CountScore(Map);
			GamerScore.Coins[season] = Coins;
			Score += GamerScore.CountScore(season);
		}

		public void SetMap(Map map)
		{
			Map = map;
		}
	}
}
