﻿namespace Cartographers.Domain
{
   public interface IDecree
   {
      DecreeCode DecreeCode { get; }
      string Name { get; }
      string DecreeDescription { get; }
      int CountScore(Map map); // make delegate
   }
}
