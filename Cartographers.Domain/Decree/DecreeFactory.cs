﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Cartographers.Domain
{
   public class DecreeFactory
   {
      private Dictionary<DecreeType, Dictionary<DecreeCode, Type>> _decrees = new Dictionary<DecreeType, Dictionary<DecreeCode, Type>>();
      private Random rnd = new Random();

      public DecreeFactory()
      {
         Init();
      }
      void Init()
      {
         var decreeType = typeof(IDecree);
         foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
         {
            if (decreeType.IsAssignableFrom(type) &&
                !type.IsInterface &&
                type.CustomAttributes.Any())
            {
               AddType(
                  ((DecreeAttribute)type.GetCustomAttribute(typeof(DecreeAttribute))).DecreeType,
                  ((DecreeAttribute)type.GetCustomAttribute(typeof(DecreeAttribute))).DecreeCode,
                  type);
            }
         }
      }

      void AddType(DecreeType type, DecreeCode code, Type t)
      {
         if (_decrees.ContainsKey(type))
         {
            _decrees[type].Add(code, t);
         }
         else
         {
            _decrees.Add(type, new Dictionary<DecreeCode,Type>{ { code, t } });
         }
      }

      public IDecree GetDecreeByCode(DecreeCode code)
         => (IDecree)Activator.CreateInstance(_decrees.SelectMany(s => s.Value).First(f => f.Key == code).Value);

      public IEnumerable<IDecree> GetDecreesByType(DecreeType type)
      {
         List<IDecree> decrees = new List<IDecree>();
         _decrees[type].Select(s => s.Value).ToList().ForEach(f => decrees.Add((IDecree)Activator.CreateInstance(f)));
         return decrees;
      }

      public IDecree GetRandomDecreeOfCertainType(DecreeType type)
         => GetDecreesByType(type).Random(rnd);
   }
}
