﻿namespace Cartographers.Domain
{
   public enum DecreeType
   {
      ForestDecree,
      WatersAndFielsDecree,
      Community,
      SquareDecree
   }
}
