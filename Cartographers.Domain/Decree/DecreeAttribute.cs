﻿using System;

namespace Cartographers.Domain
{
   public class DecreeAttribute : Attribute
   {
      public DecreeCode DecreeCode { get; set; }
      public DecreeType DecreeType { get; set; }
   }
}
