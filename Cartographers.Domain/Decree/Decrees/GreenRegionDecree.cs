﻿using System.Linq;

namespace Cartographers.Domain
{
   [Decree(DecreeCode = DecreeCode.GreenRegion, DecreeType = DecreeType.ForestDecree)]
   public class GreenRegionDecree : IDecree
    {
        private static GreenRegionDecree _instance;
        public static GreenRegionDecree Instance 
        { 
            get
            {
                if (_instance == null)
                {
                    _instance = new GreenRegionDecree();
                }
                return _instance;
            } 
        }

        public DecreeCode DecreeCode => DecreeCode.GreenRegion;

        public DecreeType DecreeType => DecreeType.ForestDecree;

        public string DecreeDescription => "Получите одну монету за каждый ряд и каждый столбец, " +
            "в которых есть хотя бы одна клетка леса. Одна и так же клетка может быть учтена одновременно " +
            "и для ряда и для столбца.";

        public string Name => "Зеленый край";

        public int Score { get; private set; }

        private GreenRegionDecree() { }

        public int CountScore(Map map)
        {
            Score = map.GetJaggedArrayXY().Where(w => w.Any(a => a == FieldType.Forest)).Count() +
                                 map.GetJaggedArrayYX().Where(w => w.Any(a => a == FieldType.Forest)).Count();
            return Score;
        }
    }
}
