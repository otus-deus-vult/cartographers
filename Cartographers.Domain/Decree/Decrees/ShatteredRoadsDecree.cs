﻿namespace Cartographers.Domain
{
   [Decree(DecreeCode = DecreeCode.ShatteredRoads, DecreeType = DecreeType.SquareDecree)]
   public class ShatteredRoadsDecree : IDecree
   {
      public DecreeCode DecreeCode => DecreeCode.ShatteredRoads;

      public string Name => "Разбитые дороги";

      public string DecreeDescription => "Получите три монеты за каждую диагональ, " +
         "полностью состоящую из заполненных клеток и касающуюся левого и нижнего краёв карты владений";

      public int Score { get; private set; }

      public int CountScore(Map map)
      {
         Score = map.GetDiagonalsLeftBottom() * 3;
         return Score;
      }
   }
}
