﻿using System.Linq;

namespace Cartographers.Domain
{
   
    public class RiverFarmDecree : IDecree
    {
        private static RiverFarmDecree _instance;
        public static RiverFarmDecree Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RiverFarmDecree();
                }
                return _instance;
            }
        }

        public DecreeCode DecreeCode => DecreeCode.RiverFarm;

        public DecreeType DecreeType => DecreeType.WatersAndFielsDecree;

        public string Name => "Приозерье";

        public string DecreeDescription => "Получите 1 монету за каждую клетку полей, примыкающую хотя бы к одной клетке" +
            "водоема. Получите 1 монету за каждую клетку водоема, примыкающую хотя бы к одной клетке полей.";

        public int Score { get; private set; }

        public int CountScore(Map map)
        {
            int Score = 0;
            int Score2 = 0;
            foreach (var field in map.GetFieldsOfCertainType(FieldType.River))
            {
                if (map.GetNeighbourFieldsOfCertainType(field, FieldType.Farm).Count() > 0)
                    Score++;
            }
            foreach (var field in map.GetFieldsOfCertainType(FieldType.Farm))
            {
                if (map.GetNeighbourFieldsOfCertainType(field, FieldType.River).Count() > 0)
                    Score++;
            }
            Score2 += map.GetFieldsOfCertainType(FieldType.River).Where(f => map.GetNeighbourFieldsOfCertainType(f, FieldType.Farm).Count() > 0).Count();
            Score2 += map.GetFieldsOfCertainType(FieldType.Farm).Where(f => map.GetNeighbourFieldsOfCertainType(f, FieldType.River).Count() > 0).Count();
            return Score;
        }
    }
}
