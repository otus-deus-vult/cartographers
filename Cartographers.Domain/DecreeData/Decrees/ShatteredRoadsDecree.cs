﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.Domain.Surfaces;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.ShatteredRoads)]
    public class ShatteredRoadsDecree : Decree
    {
        public ShatteredRoadsDecree() :
            base
            (
                code: DecreeCode.ShatteredRoads,
                name: "Разбитые дороги",
                description: "Получите три монеты за каждую диагональ, полностью состоящую из заполненных клеток и касающуюся левого и нижнего краёв карты владений."
            ) { }

        public override int CountScore(Map map)
            => map.GetFilledLeftBottomDiagonalsCount() * 3;
    }
}