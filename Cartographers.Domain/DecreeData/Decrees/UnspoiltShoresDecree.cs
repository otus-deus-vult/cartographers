﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.UnspoiltShores)]
    public class UnspoiltShoresDecree : Decree
    {
        public UnspoiltShoresDecree() :
            base
            (
                code: DecreeCode.UnspoiltShores,
                name: "Нетронутые берега",
                description: "Получите три монеты за каждую область из клеток полей, не примыкающую к клеткам водоёмов и краям карты владений. " +
                             "Получите три монеты за каждую область из клеток водоёмов, не примыкающую к клеткам полей и краям карты владений."
            ) { }

        public override int CountScore(Map map)
            => map.GetAreasWithNeighborOfType(FieldType.Farm)
                  .Count(c => c.Neighbors.All(f => f.Field != FieldType.River) && c.Fields.All(a => !a.IsBorderField(map))) * 3 +
               map.GetAreasWithNeighborOfType(FieldType.River)
                  .Count(c => c.Neighbors.All(f => f.Field != FieldType.Farm) && c.Fields.All(a => !a.IsBorderField(map))) * 3;
    }
}