﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.GreatTown)]
    public class GreatTownDecree : Decree
    {
        public GreatTownDecree() :
            base
            (
                code: DecreeCode.GreatTown,
                name: "Великий город",
                description: "Получите одну монету за каждую клетку в самой большой области из клеток поселений, не примыкающей ни к одной клетке гор."
            ) { }

        public override int CountScore(Map map)
        {
            var towns = map.GetAreasWithNeighborOfType(FieldType.Town)
                .Where(a => a.Neighbors.All(f => f.Field != FieldType.Mountain));
            return towns.Any() ? towns
                .OrderByDescending(a => a.Fields.Count)
                .First()
                .Fields
                .Count : 0;
        }
    }
}