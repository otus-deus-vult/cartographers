﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.WatchtowerForest)]
    public class WatchtowerForestDecree : Decree
    {
        public WatchtowerForestDecree() :
            base
            (
                code: DecreeCode.WatchtowerForest,
                name: "Сторожевой лес",
                description: "Получите одну монету за каждый ряд и каждый столбец, в которых есть хотя бы одна клетка леса."
            ) { }

        public override int CountScore(Map map)
            => map.GetBorderFields().Count(c => c.Field == FieldType.Forest);
    }
}
