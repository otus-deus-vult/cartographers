﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.GoldMine)]
    public class GoldMineDecree : Decree
    {
        public GoldMineDecree() :
            base
            (
                code: DecreeCode.GoldMine,
                name: "Золотая жила",
                description: "Получите одну монету за каждую клетку водоёма, примыкающую к клетке руин. " +
                             "Получите три монеты за каждую клетку полей, нарисованную на клетке руин."
            ) { }

        public override int CountScore(Map map)
            => map.GetFieldsOnMapWithNeighbor()
                  .Count(c => c.Field == FieldType.River && c.Neighbors.Any(n => n.Field == FieldType.Ruin)) +
               map.GetFieldsOnMap()
                  .Count(c => c.Field == FieldType.RuinFarm) * 3;
    }
}
