﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.RiverFarm)]
    public class RiverFarmDecree : Decree
    {
        public RiverFarmDecree() :
            base
            (
                code: DecreeCode.RiverFarm,
                name: "Приозёрье",
                description: "Получите одну монету за каждую клетку полей, примыкающую хотя бы к одной клетке водоёма. " +
                             "Получите одну монету за каждую клетку водоёма, примыкающую хотя бы к одной клетке полей."
            ) { }

        public override int CountScore(Map map)
            => map.GetFieldsOnMapWithNeighbor()
                  .Count(c => c.Field == FieldType.Farm && c.Neighbors.Any(n => n.Field == FieldType.River)) +
               map.GetFieldsOnMapWithNeighbor()
                  .Count(c => c.Field == FieldType.River && c.Neighbors.Any(n => n.Field == FieldType.Farm));
    }
}
