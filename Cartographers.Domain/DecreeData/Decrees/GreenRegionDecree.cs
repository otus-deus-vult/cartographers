﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.GreenRegion)]
    public class GreenRegionDecree : Decree
    {
        public GreenRegionDecree() :
            base
            (
                code: DecreeCode.GreenRegion,
                name: "Зеленый край",
                description: "Получите одну монету за каждую клетку леса, примыкающую к краю карты владений. " +
                             "Одна и так же клетка может быть учтена одновременно и для ряда и для столбца."
            ) { }

        public override int CountScore(Map map)
            => map.GetRows().Count(c => c.Any(a => a == FieldType.Forest || a == FieldType.RuinForest)) +
               map.GetColumns().Count(c => c.Any(a => a == FieldType.Forest || a == FieldType.RuinForest));
    }
}

