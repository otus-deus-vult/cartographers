﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.PromisedLand)]
    public class PromisedLandDecree : Decree
    {
        public PromisedLandDecree() :
            base
            (
                code: DecreeCode.PromisedLand,
                name: "Земля обетованная",
                description: "Получите три монеты за каждую область из клеток поселений, примыкающую к областям трёх или более разных типов местности."
            ) { }

        public override int CountScore(Map map)
            => map.GetAreasWithNeighborOfType(FieldType.Town)
                  .Count(c => c.Neighbors
                               .Select(n => n.Field)
                               .Where(f => f.IsTerritoryField())
                               .Distinct()
                               .Count() >= 3) * 3;
    }
}