﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.Community)]
    public class CommunityDecree : Decree
    {
        public CommunityDecree() :
            base
            (
                code: DecreeCode.Community,
                name: "Община",
                description: "Получите восемь монет за каждую область из 6 или более клеток поселений."
            ) { }

        public override int CountScore(Map map)
            => map.GetAreasOfType(FieldType.Town).Count(c => c.Fields.Count >= 6) * 8;
    }
}