﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.ImmenseExpanses)]
    public class ImmenseExpansesDecree : Decree
    {
        public ImmenseExpansesDecree() :
            base
            (
                code: DecreeCode.ImmenseExpanses,
                name: "Необъятные просторы",
                description: "Получите шесть монет за каждый ряд или столбец, полностью состоящий из заполненных клеток."
            ) { }

        public override int CountScore(Map map)
            => map.GetRows().Count(c => c.All(f => f.IsTerritoryField())) * 6 +
               map.GetColumns().Count(c => c.All(f => f.IsTerritoryField())) * 6;
    }
}