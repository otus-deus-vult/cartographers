﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.Domain.Surfaces;
using Cartographers.DTO;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.Calderas)]
    public class CalderasDecree : Decree
    {
        public CalderasDecree() :
            base
            (
                code: DecreeCode.Calderas,
                name: "Кальдеры",
                description: "Получите одну монету за каждую незаполненную клетку, окруженную с четырёх сторон заполненными клетками или краями владений."
            ) { }

        public override int CountScore(Map map)
            => map.GetFieldsOnMapWithNeighbor()
                  .Count(c => (c.Field == FieldType.Empty) && c.Neighbors.All(n => n.Field.IsTerritoryField()));
    }
}