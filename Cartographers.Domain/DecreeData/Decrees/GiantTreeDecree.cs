﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.GiantTree)]
    public class GiantTreeDecree : Decree
    {
        public GiantTreeDecree() :
            base
            (
                code: DecreeCode.GiantTree,
                name: "Дерево-великан",
                description: "Получите одну монету за каждую клетку леса, окружённую с четырех сторон заполненными клетками или краями карты владений."
            ) { }

        public override int CountScore(Map map)
            => map.GetFieldsOnMapWithNeighbor()
                  .Count(c => c.Field.IsEaual(FieldType.Forest) && c.Neighbors.All(n => n.Field.IsTerritoryField()));
    }
}
