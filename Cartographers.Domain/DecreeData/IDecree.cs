﻿using Cartographers.Domain.Surfaces;

namespace Cartographers.Domain.DecreeData
{
    public interface IDecree
    {
        DecreeCode Code { get; }
        string Name { get; }
        string Description { get; }
        int CountScore(Map map);
    }
}
