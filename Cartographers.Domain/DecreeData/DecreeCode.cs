﻿namespace Cartographers.Domain.DecreeData
{
    public enum DecreeCode
	{
		None = 0,
		WatchtowerForest = 1,
		GreenRegion = 2,
		GiantTree = 3,
		MountainForest = 4,
		RiverFarm = 5,
		WizardValley = 6,
		GoldMine = 7,
		UnspoiltShores = 8,
		Community = 9,
		GreatTown = 10,
		PromisedLand = 11,
		Outpost = 12,
		ImmenseExpanses = 13,
		LostKingdom = 14,
		ShatteredRoads = 15,
		Calderas = 16
	}
}
