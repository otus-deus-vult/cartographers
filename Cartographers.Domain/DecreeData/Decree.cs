﻿using Cartographers.Domain.Surfaces;

namespace Cartographers.Domain.DecreeData
{
	public abstract class Decree : IDecree
	{
		public DecreeCode Code { get; }
		public string Name { get; }
		public string Description { get; }
		public abstract int CountScore(Map map);

		public Decree(DecreeCode code, string name, string description)
			=> (Code, Name, Description) = (code, name, description);
	}
}
