﻿using Cartographers.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cartographers.Domain
{
    public static class Extensions
   {
		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> enumerable, Random rnd = null)
		{
			rnd = rnd ?? new Random();
			return enumerable.OrderBy(e => rnd.Next());
		}

        public static FieldType[,] GenerateEmptyFields(int Height, int Width)
        {
            if (Height > 0 && Width > 0)
            {
                FieldType[,] fieldTypes = new FieldType[Height, Width];
                for (int i = 0; i < Height; i++)
                {
                    for (int j = 0; j < Width; j++)
                    {
                        fieldTypes[i, j] = FieldType.Empty;
                    }
                }
                return fieldTypes;
            }
            else return null;
        }

        public static FieldType[,] Copy(this FieldType[,] fields)
        {
            var Height = fields.GetLength(0);
            var Width = fields.GetLength(1);
            var copy = new FieldType[Height, Width];
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    copy[i, j] = fields[i, j];
                }
            }
            return copy;
        }
    }
}
