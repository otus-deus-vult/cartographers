﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cartographers.Domain.ResearchCardData
{
	public class CardDeck
	{
		private IResearchCard[] _cards;
		private int[] _ruinIndexes;
		private int _currentTurn = 0;

		public CardDeck(IEnumerable<IResearchCard> cards, int[] ruinIndexes)
		{
			_cards = cards.ToArray();
			_ruinIndexes = ruinIndexes;
		}

		public IResearchCard GetCard()
		{
			var card = _cards[_currentTurn];
			card.IsRuin = _ruinIndexes.Any(i => i == _currentTurn);
			++_currentTurn;
			return card;
		}

		public IEnumerable<IResearchCard> GetRemainedMonsterCards()
		{
			return _cards.Skip(_currentTurn).Where(c => c.IsMonster);
		}
	}
}
