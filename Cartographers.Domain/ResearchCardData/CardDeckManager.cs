﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cartographers.Domain.SeasonData;

namespace Cartographers.Domain.ResearchCardData
{
	public class CardDeckManager
	{
		private Random rnd = new Random();
		private IEnumerable<IResearchCard> _researchCards;
		private IResearchCard[] _monsterCards;

		public CardDeckManager(ResearchCardFactory factory)
		{
			_researchCards = factory.GetAllResearchCards();
			var monsterCards = factory.GetAllMonsterCards();
			_monsterCards = monsterCards.Shuffle(rnd).ToArray();
		}

		public CardDeck CreateNextCardDeck(Season season, CardDeck previousDeck = null)
		{
			var cards = _researchCards.Append(_monsterCards[(int)season.Code]);
			if (previousDeck != null)
				cards = cards.Concat(previousDeck.GetRemainedMonsterCards());
			cards = cards.Shuffle(rnd).ToArray();
			int[] ruinIndexes = cards.Select((s, index) => new { IsMonster = s.IsMonster, Index = index })
									.Where(w => !w.IsMonster)
									.Select(s => s.Index)
									.Shuffle(rnd).Take(2).ToArray();
			return new CardDeck(cards, ruinIndexes);
		}
	}
}
