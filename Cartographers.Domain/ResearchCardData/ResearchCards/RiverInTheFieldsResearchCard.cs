﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.RiverInTheFields)]
   class RiverInTheFieldsResearchCard : ResearchCard
   {
      public RiverInTheFieldsResearchCard()
      {
         Code = ResearchCode.RiverInTheFields;
         Name = "Река в полях";
         MovePoints = 2;
         Fields = new FieldType[]
         {
            FieldType.Farm,
            FieldType.River
         };
         bool[,] shape540 = new bool[3, 3];
         shape540[0, 2] = true;
         shape540[0, 1] = true;
         shape540[0, 0] = true;
         shape540[1, 2] = true;
         shape540[2, 2] = true;
         Shapes = new Shape[]
         {
            new Shape(){ Contour = shape540 }
         };
      }
   }
}