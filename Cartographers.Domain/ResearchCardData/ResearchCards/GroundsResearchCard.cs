﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.Grounds)]
   class GroundsResearchCard : ResearchCard
   {
      public GroundsResearchCard()
      {
         Code = ResearchCode.Grounds;
         Name = "Угодья";
         MovePoints = 1;
         Fields = new FieldType[]
         {
            FieldType.Farm
         };
         bool[,] shape211 = new bool[1, 2];
         shape211[0, 1] = true;
         shape211[0, 0] = true;
         bool[,] shape510 = new bool[3, 3];
         shape510[1, 0] = true;
         shape510[1, 1] = true;
         shape510[1, 2] = true;
         shape510[0, 1] = true;
         shape510[2, 1] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape211, HasGoldCoin = true },
            new Shape() { Contour = shape510 }
         };
      }
   }
}
