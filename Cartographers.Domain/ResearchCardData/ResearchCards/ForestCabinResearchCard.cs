﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.ForestCabin)]
   class ForestCabinResearchCard : ResearchCard
   {
      public ForestCabinResearchCard()
      {
         Code = ResearchCode.ForestCabin;
         Name = "Лесные хижины";
         MovePoints = 2;
         Fields = new FieldType[]
         {
            FieldType.Forest,
            FieldType.Town
         };
         bool[,] shape560 = new bool[4, 2];
         shape560[0, 0] = true;
         shape560[1, 0] = true;
         shape560[2, 0] = true;
         shape560[2, 1] = true;
         shape560[3, 1] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape560 }
         };
      }
   }
}