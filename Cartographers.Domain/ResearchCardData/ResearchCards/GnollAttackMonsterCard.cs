﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.GnollAttack, IsMonster = true)]
   class GnollAttackMonsterCard : ResearchCard
   {
      public GnollAttackMonsterCard()
      {
         Code = ResearchCode.GnollAttack;
         Name = "Набег гноллов";
         MovePoints = 0;
         Fields = new FieldType[]
         {
            FieldType.Monster
         };
         bool[,] shape500 = new bool[2, 3];
         shape500[0, 2] = true;
         shape500[0, 1] = true;
         shape500[0, 0] = true;
         shape500[1, 2] = true;
         shape500[1, 0] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape500 }
         };
      }
   }
}
