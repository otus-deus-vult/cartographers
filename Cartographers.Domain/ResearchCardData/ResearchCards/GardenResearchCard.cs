﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.Garden)]
   class GardenResearchCard : ResearchCard
   {
      public GardenResearchCard()
      {
         Code = ResearchCode.Garden;
         Name = "Сад";
         MovePoints = 2;
         Fields = new FieldType[]
         {
            FieldType.Forest,
            FieldType.Farm
         };
         bool[,] shape420 = new bool[3, 2];
         shape420[0, 1] = true;
         shape420[1, 1] = true;
         shape420[2, 1] = true;
         shape420[2, 0] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape420 }
         };
      }
   }
}
