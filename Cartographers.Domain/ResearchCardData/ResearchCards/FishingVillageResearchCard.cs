﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.FishingVillage)]
   class FishingVillageResearchCard : ResearchCard
   {
      public FishingVillageResearchCard()
      {
         Code = ResearchCode.FishingVillage;
         Name = "Рыбацкая деревня";
         MovePoints = 2;
         Fields = new FieldType[]
         {
            FieldType.Town,
            FieldType.River
         };
         bool[,] shape430 = new bool[4, 1];
         shape430[0, 0] = true;
         shape430[1, 0] = true;
         shape430[2, 0] = true;
         shape430[3, 0] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape430 }
         };
      }
   }
}
