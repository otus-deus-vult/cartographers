﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.FarmStead)]
   class FarmSteadResearchCard : ResearchCard
   {
      public FarmSteadResearchCard()
      {
         Code = ResearchCode.FarmStead;
         Name = "Хутор";
         MovePoints = 2;
         Fields = new FieldType[]
         {
            FieldType.Town,
            FieldType.Farm
         };
         bool[,] shape410 = new bool[2, 3];
         shape410[0, 2] = true;
         shape410[0, 1] = true;
         shape410[0, 0] = true;
         shape410[1, 1] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape410 }
         };
      }
   }
}
