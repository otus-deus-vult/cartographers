﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    class AnomalyMonsterCard : ResearchCard
   {
      public AnomalyMonsterCard()
      {
         Code = ResearchCode.Anomaly;
         Name = "Аномалия";
         MovePoints = 0;
         Fields = new FieldType[]
         {
            FieldType.Forest,
            FieldType.Town,
            FieldType.Farm,
            FieldType.River,
            FieldType.Monster
         };
         bool[,] shape100 = new bool[1, 1];
         shape100[0, 0] = true;
         Shapes = new Shape[]
         {
            new Shape() {Contour = shape100}
         };
      }
   }
}
