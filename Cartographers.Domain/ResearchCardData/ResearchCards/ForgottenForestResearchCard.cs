﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.ForgottenForest)]
   class ForgottenForestResearchCard : ResearchCard
   {
      public ForgottenForestResearchCard()
      {
         Code = ResearchCode.ForgottenForest;
         Name = "Забытый лес";
         MovePoints = 1;
         Fields = new FieldType[]
         {
            FieldType.Forest
         };
         bool[,] shape201 = new bool[2, 2];
         shape201[0, 1] = true;
         shape201[1, 0] = true;
         bool[,] shape440 = new bool[2, 3];
         shape440[0, 2] = true;
         shape440[0, 1] = true;
         shape440[1, 1] = true;
         shape440[1, 0] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape201, HasGoldCoin = true },
            new Shape() { Contour = shape440 }
         };
      }
   }
}
