﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.BugbearAttack, IsMonster = true)]
   class BugbearAttackMonsterCard : ResearchCard
   {
      public BugbearAttackMonsterCard()
      {
         Code = ResearchCode.BugbearAttack;
         Name = "Натиск багберов";
         MovePoints = 0;
         Fields = new FieldType[]
         {
            FieldType.Monster
         };
         bool[,] shape400 = new bool[3, 2];
         shape400[0, 1] = true;
         shape400[0, 0] = true;
         shape400[2, 1] = true;
         shape400[2, 0] = true;
         Shapes = new Shape[]
         {
            new Shape() {Contour = shape400}
         };
      }
   }
}
