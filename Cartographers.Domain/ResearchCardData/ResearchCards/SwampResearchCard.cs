﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.Swamp)]
   class SwampResearchCard : ResearchCard
   {
      public SwampResearchCard()
      {
         Code = ResearchCode.Swamp;
         Name = "Болото";
         MovePoints = 2;
         Fields = new FieldType[]
         {
            FieldType.Forest,
            FieldType.River
         };
         bool[,] shape550 = new bool[3, 3];
         shape550[0, 2] = true;
         shape550[0, 1] = true;
         shape550[0, 0] = true;
         shape550[1, 1] = true;
         shape550[2, 1] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape550 }
         };
      }
   }
}