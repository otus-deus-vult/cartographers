﻿using System;

namespace Cartographers.Domain.ResearchCardData
{
   class ResearchCardAttribute : Attribute
   {
      public ResearchCode ResearchCode { get; set; }
      public bool IsMonster { get; set; } = false;
   }
}
