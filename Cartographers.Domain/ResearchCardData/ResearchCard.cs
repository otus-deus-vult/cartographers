﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    public abstract class ResearchCard : IResearchCard
	{
		public ResearchCode Code { get; internal set; }
		public FieldType[] Fields { get; internal set; }
		public Shape[] Shapes { get; internal set; }
		public string Name { get; internal set; }
		public int MovePoints { get; internal set; }
		public bool IsRuin { get; set; }
		public bool IsMonster => Fields[0] == FieldType.Monster;
		public static bool operator ==(ResearchCard a, ResearchCard b) => a.Code == b.Code;
		public static bool operator !=(ResearchCard a, ResearchCard b) => !(a == b);
		public bool Equals(IResearchCard other) => other != null && (ResearchCard)other == this;
		public override int GetHashCode() => Code.GetHashCode();
	}
}
