﻿namespace Cartographers.Domain.ResearchCardData
{
   public enum ResearchCode
   {
      None              = 0,
      GreatRiver        = 1,
      Grounds           = 2,
      Township          = 3,
      ForgottenForest   = 4,
      RiverInTheFields  = 5,
      FarmStead         = 6,
      Garden            = 7,
      ForestCabin       = 8,
      Swamp             = 9,
      FishingVillage    = 10,
      GoblinAttack      = 11,
      BugbearAttack     = 12,
      CoboldAttack      = 13,
      GnollAttack       = 14,
      Anomaly           = 15,
      AnomalyMonster    = 16
   }
}
