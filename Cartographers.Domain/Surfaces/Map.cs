﻿using Cartographers.DTO;
using Cartographers.DTO;

namespace Cartographers.Domain.Surfaces
{
	public class Map: Board
	{
		public const int DefaultMapSize = 11;

		public Map(FieldType[,] fields): base(fields)
		{ }

		public Map(BoardDTO boardDto): base(boardDto)
		{ }

		public bool CanAdd(Figure figure, Point position, bool isRuinCard = false)
		{
			bool includesRuinField = false;
			if (position.X < 0 || position.Y < 0) return false;
			if (figure.Width + position.X > Width) return false;
			if (figure.Height + position.Y > Height) return false;
			for (int i = 0; i < figure.Width; i++)
				for (int j = 0; j < figure.Height; j++)
				{
					if (figure[i, j] != FieldType.Empty &&
						figure[i, j] != FieldType.None &&
						this[i + position.X, j + position.Y] != FieldType.Empty &&
						this[i + position.X, j + position.Y] != FieldType.Ruin)
						return false;
					if (figure[i, j] != FieldType.None &&
						this[i + position.X, j + position.Y] == FieldType.Ruin)
						includesRuinField = true;
				}
			if (isRuinCard && !includesRuinField) return false;
			return true;
		}

		public void Move(Figure figure, Point position)
		{
			for (int i = 0; i < figure.Width; i++)
				for (int j = 0; j < figure.Height; j++)
				{
					if (figure[i, j] != FieldType.Empty &&
						figure[i, j] != FieldType.None)
					{
						FieldType field = figure[i, j];
						if (this[i + position.X, j + position.Y] == FieldType.Ruin)
						{
							switch(figure[i, j])
							{
								case FieldType.Farm:
									field = FieldType.RuinFarm;
									break;
								case FieldType.Forest:
									field = FieldType.RuinForest;
									break;
								case FieldType.River:
									field = FieldType.RuinRiver;
									break;
								case FieldType.Town:
									field = FieldType.RuinTown;
									break;
								case FieldType.Monster:
									field = FieldType.RuinMonster;
									break;
							}
						}
						this[i + position.X, j + position.Y] = field;
					}
				}
		}

		public static implicit operator Map(FieldType[,] fields) => new Map(fields);
	}
}
