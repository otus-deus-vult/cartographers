﻿using Cartographers.DTO;
using Cartographers.Domain.ResearchCardData;

namespace Cartographers.Domain.Surfaces
{
	public class Figure: Board
	{
		public Figure(FieldType[,] fields): base(fields)
		{}

		public Figure(Shape shape, FieldType type): base(shape, type)
		{}

		public void Rotate()
		{
			var newFields = new FieldType[Height, Width];
			for (int i = 0; i < Width; ++i)
				for (int j = 0; j < Height; ++j)
				{
					newFields[j, Width - i - 1] = _fields[i, j];
				}
			_fields = newFields;
			(Height, Width) = (Width, Height);
		}

		public void ReflectHorizontal()
		{
			var newFields = new FieldType[Width, Height];
			for (int i = 0; i < Width; ++i)
				for (int j = 0; j < Height; ++j)
				{
					newFields[i, Height - j - 1] = _fields[i, j];
				}
			_fields = newFields;
		}

		public void ReflectVertical()
		{
			var newFields = new FieldType[Width, Height];
			for (int i = 0; i < Width; ++i)
				for (int j = 0; j < Height; ++j)
				{
					newFields[Width - i - 1, j] = _fields[i, j];
				}
			_fields = newFields;
		}

		public static implicit operator bool[,](Figure figure)
		{
			int dimX = figure._fields.GetLength(0);
			int dimY = figure._fields.GetLength(1);
			bool[,] ret = new bool[dimX, dimY];
			for (int x = 0; x < dimX; x++)
			{
				for (int y = 0; y < dimY; y++)
				{
					ret[x, y] = figure._fields[x, y] != FieldType.None &&
								figure._fields[x, y] != FieldType.Empty;
				}
			}
			return ret;
		}
	}
}
