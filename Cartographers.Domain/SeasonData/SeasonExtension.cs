﻿namespace Cartographers.Domain.SeasonData
{
	public static class SeasonExtension
	{
		public static bool IsEndSeason(this Season season)
		{
			return season.Code == SeasonCode.Winter;
		}

		public static bool IsEnded(this Season season, int turn)
		{
			return season.Duration <= turn;
		}
	}
}
