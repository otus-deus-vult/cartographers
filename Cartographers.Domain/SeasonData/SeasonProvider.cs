﻿using System.Collections.Generic;

namespace Cartographers.Domain.SeasonData
{
   public static class SeasonProvider
   {
		private static readonly Dictionary<SeasonCode, Season> _seasons =  new Dictionary<SeasonCode, Season>();
		static SeasonProvider()
		{
			_seasons.Add(SeasonCode.Spring, new Season(SeasonCode.Spring, "Весна", 8));
			_seasons.Add(SeasonCode.Summer, new Season(SeasonCode.Summer, "Лето", 8));
			_seasons.Add(SeasonCode.Fall, new Season(SeasonCode.Fall, "Осень", 7));
			_seasons.Add(SeasonCode.Winter, new Season(SeasonCode.Winter, "Зима", 6));
		}

		public static Season GetSeasonByCode(SeasonCode code)
		{
			return _seasons[code];
		}

		public static Season GetSeasonByIndex(int index)
		{
			return GetSeasonByCode((SeasonCode)index);
		}
   }
}
