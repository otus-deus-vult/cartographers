﻿namespace Cartographers.Domain.SeasonData
{
    public class Season
	{
		public SeasonCode Code { get; private set; }
		public string Name { get; private set; }
		public int Duration { get; private set; }

		public Season(SeasonCode code, string name, int duration)
		{
			Code = code;
			Name = name;
			Duration = duration;
		}

		public override int GetHashCode() => Code.GetHashCode();
		public static bool operator ==(Season a, Season b) => a.Code == b.Code;
		public static bool operator !=(Season a, Season b) => !(a == b);

		public override bool Equals(object obj)
			=> obj != null &&
				obj.GetType() == typeof(Season) &&
				(Season)obj == this;
	}
}
