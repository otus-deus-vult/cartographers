﻿namespace Cartographers.DTO
{
	public class BoardDTO
	{
		public int Width { get; set; }
		public int Height { get; set; }

		public FieldType[] Fields { get; set; }
	}
}
