﻿namespace Cartographers.DTO
{
    public class SessionSettings
    {
        public string SessionUid { get; set; }
        public string SessionOwner { get; set; }
        public int TurnDuration { get; set; }
    }
}
