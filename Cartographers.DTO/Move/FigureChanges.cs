﻿namespace Cartographers.DTO
{
    public class FigureChanges
	{
		public int RotateCount { get; set; }
		public bool IsReflectedHorizontal { get; set; }
		public bool IsReflectedVertical { get; set; }
		public int ShapeIndex { get; set; }
		public int FieldIndex { get; set; }
	}
}
