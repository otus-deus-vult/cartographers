﻿namespace Cartographers.DTO.Db
{
    public class Status
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}
