﻿using System;

namespace Cartographers.DTO.Db
{
    public class Game
    {
        public int Id { get; set; }
        public string SessionUid { get ; set; }
        public int GamerId { get; set; }
        public int StatusId { get; set; }
        public int Score { get; set; }
        public DateTime Date { get; set; }
        public Session Session { get; set; }
        public Gamer Gamer { get; set; }
        public Status Status { get; set; }
    }
}
