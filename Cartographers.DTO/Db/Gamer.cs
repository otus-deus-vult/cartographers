﻿using System;

namespace Cartographers.DTO.Db
{
    public class Gamer : IComparable<Gamer>
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public int CompareTo(Gamer other) => this.Login.CompareTo(other.Login);
    }
}
