﻿using System;

namespace Cartographers.DTO.Db
{
    public class Session
    {
        public string Uid { get; set; }
        public DateTime Date { get; set; }
    }
}
