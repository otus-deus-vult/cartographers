﻿namespace Cartographers.DTO
{
    public class GameWithGamerInfo : GamerInfo
    {
        public GameDTO Game { get; set; }
    }
}
