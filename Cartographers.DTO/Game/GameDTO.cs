﻿namespace Cartographers.DTO
{
    public class GameDTO
	{
		public BoardDTO Board { get; set; }
		public int Score { get; set; }
		public int SeasonId { get; set; }
		public int SeasonTurn { get; set; }
		public int[] DecreeIds { get; set; } // int[4]
		public int ResearchCardId { get; set; }
		public bool IsRuinCard { get; set; }
		public bool TurnAvailable { get; set; }
		public int TurnTimeLeft { get; set; }
		public bool GameCancelled { get; set; }
		public int Coins { get; set; }
		public int Monsters { get; set; }
	}
}
