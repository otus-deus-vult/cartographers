﻿namespace Cartographers.DTO
{
	public enum FieldType
	{
		Empty, // пусто - незанятая клетка карты, на которую можно поставить
		None, // пусто с прозрачным конуторм - для фигуры на карте исследований чтобы пустые области не перекрывали при переносе саму карту

		River,
		Forest,
		Ground,
		Town,
		Farm,
		Monster,
		Precipice,
		Ruin,
		Mountain,

		RuinRiver,
		RuinForest,
		RuinTown,
		RuinFarm,
		RuinMonster,
	}

	public static class FieldTypeExtensions
	{
		public static bool IsEaual(this FieldType a, FieldType b)
			=> a == b || a.ToString() == "Ruin" + b.ToString() || "Ruin" + a.ToString() == b.ToString();
	}
}
