﻿using System.Linq;

namespace Cartographers.DTO
{
    public class GamerScore
    {
        public string GamerLogin { get; set; }
        public string SessionUid { get; set; }
        public string Status { get; set; }
        public int[] DecreeScore { get; set; } = new int[8];
        public int[] Coins { get; set; } = new int[4];
        public int[] Monsters { get; set; } = new int[4];

        public int CountScore(int index)
            => Coins[index] +
               DecreeScore[index * 2] +
               DecreeScore[index * 2 + 1] -
               Monsters[index];
        public int CountScore()
        {
            int score = 0;
            new int[] { 0, 1, 2, 3 }.ToList().ForEach(f => score += CountScore(f));
            return score;
        }
    }
}
