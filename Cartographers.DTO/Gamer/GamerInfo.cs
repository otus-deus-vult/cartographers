﻿namespace Cartographers.DTO
{
    public class GamerInfo
    {
        public string SessionId { get; set; }
        public string GamerLogin { get; set; }
    }
}
