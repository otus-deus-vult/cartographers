﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;
using System.Linq;

namespace Cartographers.Domain.Tests.ExtensionTests
{
    class GetNeighboursTests
    {
        [Test]
        public void GetNeigboursCornerField()
        {
            
           var fields = Extensions.GenerateEmptyFields(4, 4);
           fields[0, 0] = FieldType.Farm;

           Map map = new Map(fields);
           var neighb = Cartographers.Domain.DecreeData.DecreeHelpers.Extensions.GetNeighbors(map, map.GetFieldsOnMap().First());

           Assert.AreEqual(2, neighb.Count);
        }
        
        [Test]
        public void GetNeigboursCenterField()
        {

            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.Farm;

            Map map = new Map(fields);
            var neighb = Cartographers.Domain.DecreeData.DecreeHelpers.Extensions.GetNeighbors(map, map.GetFieldsOnMap().First(f=>f.Field == FieldType.Farm));

            Assert.AreEqual(4, neighb.Count);
        }

        [Test]
        public void GetNeigboursBorderField()
        {

            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Farm;

            Map map = new Map(fields);
            var neighb = Cartographers.Domain.DecreeData.DecreeHelpers.Extensions.GetNeighbors(map, map.GetFieldsOnMap().First(f => f.Field == FieldType.Farm));

            Assert.AreEqual(3, neighb.Count);
        }

    }

}