﻿using NUnit.Framework;
using Cartographers.Domain.Surfaces;
using Cartographers.Domain.DecreeData;
using Cartographers.DTO;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_CalderasTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_EmtyFields_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Calderas).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1Area_1Score()
        {
            int expectedScore = 1;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Calderas).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_2Areas_2Scores()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[3, 3] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Calderas).CountScore(map);
            Assert.AreEqual(2, actualScore);
        }

        [Test]
        public void CountScores_4Areas_4Scores()
        {
            int expectedScore = 4;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            fields[3, 2] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Calderas).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_8Areas_8Scores()
        {
            int expectedScore = 8;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            fields[3, 2] = FieldType.Town;
            fields[0, 3] = FieldType.Town;
            fields[3, 0] = FieldType.Town;

            Map map = new Map(fields);

            var actualScore = _df.GetByCode(DecreeCode.Calderas).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

    }
}
