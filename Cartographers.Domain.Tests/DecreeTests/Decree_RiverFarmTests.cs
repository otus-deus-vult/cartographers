﻿using NUnit.Framework;
using Cartographers.Domain.Surfaces;
using Cartographers.Domain.DecreeData;
using Cartographers.DTO;

namespace Cartographers.Domain.Tests.DecreeTests
{

    public class Decree_RiverFarmTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_ReturnsZeroScore()
        {
            int Score = 0;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Empty, FieldType.Empty }, { FieldType.Empty, FieldType.Empty } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.RiverFarm).CountScore(map);

            Assert.AreEqual(Score, actualScore);
        }

        [Test]
        public void CountScores_Returns2Scores()
        {
            int Score = 2;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Empty, FieldType.Empty }, { FieldType.Farm, FieldType.River } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.RiverFarm).CountScore(map);

            Assert.AreEqual(Score, actualScore);
        }

        [Test]
        public void CountScores_2DiagonalForests_Returns4Scores()
        {
            int Score = 0;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Empty, FieldType.Farm }, { FieldType.River, FieldType.Empty } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.RiverFarm).CountScore(map);

            Assert.AreEqual(Score, actualScore);
        }

        [Test]
        public void CountScores_2FarmRiver_Returns4Scores()
        {
            int Score = 4;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Farm, FieldType.River }, { FieldType.Farm, FieldType.River } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.RiverFarm).CountScore(map);

            Assert.AreEqual(Score, actualScore);
        }

        [Test]
        public void CountScores_Returns4Scores()
        {
            int Score = 4;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Farm, FieldType.River }, { FieldType.River, FieldType.Farm } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.RiverFarm).CountScore(map);

            Assert.AreEqual(Score, actualScore);
        }
    }
}