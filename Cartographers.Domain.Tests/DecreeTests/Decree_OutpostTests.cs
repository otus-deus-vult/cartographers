﻿using NUnit.Framework;
using Cartographers.Domain.Surfaces;
using Cartographers.Domain.DecreeData;
using Cartographers.DTO;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_OutpostTests
    {
        IDecree _outpost;

        [SetUp]
        public void Setup()
        {
            _outpost = new DecreeFactory().GetByCode(DecreeCode.Outpost);
        }

        [Test]
        public void CountScores_EmtyFields_ReturnsZeroScore()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);

            Map map = new Map(fields);

            Assert.AreEqual(0, _outpost.CountScore(map));
        }

        [Test]
        public void CountScores_1Area_ReturnsZeroScore()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;

            Map map = new Map(fields);

            Assert.AreEqual(0, _outpost.CountScore(map));
        }

        [Test]
        public void CountScores_2Areas_Returns4Scores()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Town;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            fields[3, 3] = FieldType.Town;

            Map map = new Map(fields);

            Assert.AreEqual(4, _outpost.CountScore(map));
        }

        [Test]
        public void CountScores_2SameAreas_Returns6Scores()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.Town;
            fields[1, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            fields[3, 3] = FieldType.Town;

            Map map = new Map(fields);

            Assert.AreEqual(6, _outpost.CountScore(map));
        }

        [Test]
        public void CountScores_3Areas_Returns4Scores()
        {
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.Town;
            fields[1, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[3, 3] = FieldType.Town;
            fields[3, 4] = FieldType.Town;

            Map map = new Map(fields);

            Assert.AreEqual(4, _outpost.CountScore(map));
        }

        [Test]
        public void CountScore_HasRuinTownInSmallerPart_Counts4()
        {
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.Town;
            fields[1, 1] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[3, 3] = FieldType.RuinTown;
            fields[3, 4] = FieldType.Town;

            Map map = new Map(fields);

            Assert.AreEqual(4, _outpost.CountScore(map));
        }

        [Test]
        public void CountScore_HasRuinTownInLargerPart_Counts4()
        {
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.RuinTown;
            fields[1, 1] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[3, 3] = FieldType.Town;
            fields[3, 4] = FieldType.Town;

            Map map = new Map(fields);

            Assert.AreEqual(4, _outpost.CountScore(map));
        }
    }
}
