﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_ShatteredRoads
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void Test_Decree_ShatteredRoads_SimpleCount()
        {

            FieldType[,] fieldTypes = new FieldType[3, 3]
           {
                { FieldType.Empty, FieldType.Empty, FieldType.Farm  },
                { FieldType.Empty, FieldType.River, FieldType.Empty },
                { FieldType.Town,  FieldType.Empty, FieldType.Empty }
           };
            Map map = new Map(fieldTypes);
            var scores = _df.GetByCode(DecreeCode.ShatteredRoads).CountScore(map);
            Assert.AreEqual(3, scores);
        }

        [Test]
        public void Test_Decree_ShatteredRoads_CountsMany()
        {
            var fieldTypes = Extensions.GenerateEmptyFields(4, 4);
            fieldTypes[0, 0] = FieldType.Farm;
            fieldTypes[0, 1] = FieldType.River;
            fieldTypes[1, 0] = FieldType.Town;
            fieldTypes[0, 2] = FieldType.Mountain;
            fieldTypes[1, 1] = FieldType.Monster;
            fieldTypes[2, 0] = FieldType.Town;
            Map map = new Map(fieldTypes);
            var scores = _df.GetByCode(DecreeCode.ShatteredRoads).CountScore(map);
            Assert.AreEqual(9, scores);
        }

        [Test]
        public void Test_Decree_ShatteredRoads_CountWithMountain()
        {
            var fieldTypes = Extensions.GenerateEmptyFields(5, 5);
            fieldTypes[0, 4] = FieldType.Farm;
            fieldTypes[1, 3] = FieldType.Farm;
            fieldTypes[2, 2] = FieldType.Farm;
            fieldTypes[3, 1] = FieldType.Farm;
            fieldTypes[4, 0] = FieldType.Farm;
            Map map = new Map(fieldTypes);
            var scores = _df.GetByCode(DecreeCode.ShatteredRoads).CountScore(map);
            Assert.AreEqual(3, scores);
        }

        [Test]
        public void Test_Decree_ShatteredRoads_DoesNotCountRuin()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Ruin;
            fields[1, 1] = FieldType.RuinFarm;
            fields[2, 2] = FieldType.Ruin;
            fields[3, 3] = FieldType.Ruin;
            var scores = _df.GetByCode(DecreeCode.ShatteredRoads).CountScore(new Map(fields));
            Assert.AreEqual(0, scores);
        }

        [Test]
        public void Test_Decree_ShatteredRoads_CountsRuinFarm()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 3] = FieldType.Farm;
            fields[1, 2] = FieldType.RuinFarm;
            fields[2, 1] = FieldType.Farm;
            fields[3, 0] = FieldType.Farm;
            var scores = _df.GetByCode(DecreeCode.ShatteredRoads).CountScore(new Map(fields));
            Assert.AreEqual(3, scores);
        }
    }
}
