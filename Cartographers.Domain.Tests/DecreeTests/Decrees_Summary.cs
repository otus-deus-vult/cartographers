﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decrees_Summary
    {
        DecreeFactory _df;
        Map _map;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
            _map = new Map(Extensions.GenerateEmptyFields(11, 11));
        }

        [Test]
        public void DecreeCountScore_EmptyMap_ShouldCountZero()
        {
            foreach(var decree in _df)
            {
                Assert.AreEqual(0, decree.CountScore(_map));
            }
        }
    }
}
