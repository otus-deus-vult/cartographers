﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_GiantTreeTests
    {
        IDecree _gaintTree;

        [SetUp]
        public void Setup()
        {
            _gaintTree = new DecreeFactory().GetByCode(DecreeCode.GiantTree);
        }

        [Test]
        public void CountScores_EmptyFields_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            Map map = new Map(fields);
            var actualScore = _gaintTree.CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1SCore_ReturnsZeroScore()
        {
            int expectedScore = 1;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.Forest;
            fields[0, 1] = FieldType.Farm;
            fields[2, 1] = FieldType.Town;
            fields[1, 2] = FieldType.RuinRiver;
            fields[1, 0] = FieldType.Forest;
            Map map = new Map(fields);
            var actualScore = _gaintTree.CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_FOrestNotFilledFields_0Score()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.Forest;
            fields[0, 1] = FieldType.Farm;
            fields[2, 1] = FieldType.Town;
            fields[1, 2] = FieldType.RuinRiver;
            fields[1, 0] = FieldType.Ruin;
            Map map = new Map(fields);
            var actualScore = _gaintTree.CountScore(map);
            Assert.AreEqual(0, actualScore);
        }

        [Test]
        public void CountScores_ChessLikeFields_0Scores()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Forest;
            fields[1, 0] = FieldType.Forest;
            fields[2, 1] = FieldType.Forest;
            fields[1, 2] = FieldType.Forest;
            fields[2, 3] = FieldType.Forest;
            fields[3, 2] = FieldType.Forest;
            fields[0, 3] = FieldType.Forest;
            fields[3, 0] = FieldType.Forest;
            Map map = new Map(fields);
            var actualScore = _gaintTree.CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_Coners_1Score()
        {
            int expectedScore = 1;
            var fields = Extensions.GenerateEmptyFields(3, 3);
            fields[0, 0] = FieldType.Forest;
            fields[0, 1] = FieldType.Town;
            fields[1, 0] = FieldType.Town;

            Map map = new Map(fields);

            var actualScore = _gaintTree.CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_5Scores()
        {
            int expectedScore = 5;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 0] = FieldType.Forest;
            fields[0, 1] = FieldType.Forest;
            fields[0, 3] = FieldType.Forest;
            fields[0, 4] = FieldType.Forest;
            fields[1, 0] = FieldType.Forest;
            fields[1, 2] = FieldType.Forest;
            fields[1, 4] = FieldType.Forest;
            fields[2, 1] = FieldType.Forest;
            fields[2, 2] = FieldType.Forest;
            fields[2, 3] = FieldType.Forest;
            fields[3, 0] = FieldType.Forest;
            fields[3, 2] = FieldType.Forest;
            fields[3, 4] = FieldType.Forest;
            fields[4, 0] = FieldType.Forest;
            fields[4, 1] = FieldType.Forest;
            fields[4, 3] = FieldType.Forest;
            fields[4, 4] = FieldType.Forest;

            Map map = new Map(fields);

            var actualScore = _gaintTree.CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScore_SurroundedRuinForestField_Counts1()
        {
            var fields = Extensions.GenerateEmptyFields(3, 3);
            fields[0, 0] = FieldType.RuinForest;
            fields[0, 1] = FieldType.Town;
            fields[1, 0] = FieldType.Town;

            Map map = new Map(fields);

            Assert.AreEqual(1, _gaintTree.CountScore(map));
        }
    }
}
