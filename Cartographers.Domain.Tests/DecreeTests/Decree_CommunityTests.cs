﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_CommunityTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_EmtyFields_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Community).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1Area_8Score()
        {
            int expectedScore = 8;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[3, 1] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Community).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_2Areas_16Score()
        {
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.Town;
            fields[0, 2] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;

            fields[2, 3] = FieldType.Town;
            fields[3, 3] = FieldType.Town;
            fields[4, 3] = FieldType.Town;
            fields[2, 4] = FieldType.Town;
            fields[3, 4] = FieldType.Town;
            fields[4, 4] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Community).CountScore(map);
            Assert.AreEqual(16, actualScore);
        }

        [Test]
        public void CountScores_2Areas_8Score()
        {
            int expectedScore = 8;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.Town;
            fields[0, 2] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;

            fields[2, 3] = FieldType.Town;
            fields[3, 3] = FieldType.Town;
            fields[4, 3] = FieldType.Town;
            fields[0, 4] = FieldType.Town;
            fields[3, 4] = FieldType.River;
            fields[4, 4] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.Community).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_3DiffAreas_24Scores()
        {
            int expectedScore = 24;
            var fields = Extensions.GenerateEmptyFields(6, 6);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.Town;
            fields[0, 2] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;

            fields[3, 1] = FieldType.Town;
            fields[3, 2] = FieldType.Town;
            fields[4, 1] = FieldType.Town;
            fields[4, 2] = FieldType.Town;
            fields[5, 1] = FieldType.Town;
            fields[5, 2] = FieldType.Town;

            fields[3, 4] = FieldType.Town;
            fields[3, 5] = FieldType.Town;
            fields[4, 4] = FieldType.Town;
            fields[4, 5] = FieldType.Town;
            fields[5, 4] = FieldType.Town;
            fields[5, 5] = FieldType.Town;


            Map map = new Map(fields);

            var actualScore = _df.GetByCode(DecreeCode.Community).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

    }
}
