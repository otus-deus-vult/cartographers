﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.Surfaces;
using Cartographers.DTO;
using NUnit.Framework;
using System.Collections.Generic;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_UnspoiltShoresTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_EmtyFields_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.UnspoiltShores).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1Area_3Score()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.River;
            fields[1, 2] = FieldType.River;

            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.UnspoiltShores).CountScore(map);
            Assert.AreEqual(3, actualScore);
        }

        [Theory]
        public void CountScores_0Scores((FieldType[,] fields, int expected) set)
        {
            Map map = new Map(set.fields);
            var actualScore = _df.GetByCode(DecreeCode.UnspoiltShores).CountScore(map);
            Assert.AreEqual(set.expected, actualScore);
        }

        [Test]
        public void CountScores_2Areas_6Scores()
        {
            int expectedScore = 6;
            var fields = Extensions.GenerateEmptyFields(6, 6);
            fields[1, 1] = FieldType.River;
            fields[1, 2] = FieldType.River;
            fields[3, 1] = FieldType.Farm;
            fields[3, 2] = FieldType.Farm;
            fields[3, 3] = FieldType.Farm;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.UnspoiltShores).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [DatapointSource]
        public IEnumerable<(FieldType[,], int)> Data()
        {
            var set1 = Extensions.GenerateEmptyFields(4, 4);
            var set2 = set1.Copy();

            set1[1, 1] = FieldType.Farm;
            set1[0, 0] = FieldType.River;
            set1[3, 2] = FieldType.Farm;
            set1[3, 3] = FieldType.Farm;

            set2[1, 1] = FieldType.River;
            set2[1, 2] = FieldType.Farm;
            set2[2, 2] = FieldType.Farm;

            return new List<(FieldType[,], int)>{ (set1, 0), (set2, 0) };
        }
    }
}
