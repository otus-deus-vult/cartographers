﻿using NUnit.Framework;
using Cartographers.Domain.Surfaces;
using Cartographers.Domain.DecreeData;
using Cartographers.DTO;

namespace Cartographers.Domain.Tests.DecreeTests
{
    [TestFixture]
    public class Decree_GreenRegionTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }
        [Test]
        public void CountScores_NoForest_ReturnsZeroScore()
        {
            int expectedScore = 0;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Empty, FieldType.Empty }, { FieldType.Empty, FieldType.Empty } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.GreenRegion).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1Forest_Returns2Scores()
        {
            int expectedScore = 2;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Empty, FieldType.Empty }, { FieldType.Forest, FieldType.Empty } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.GreenRegion).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_2DiagonalForests_Returns4Scores()
        {
            int expectedScore = 4;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Empty, FieldType.Forest }, { FieldType.Forest, FieldType.Empty } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.GreenRegion).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_2NotDiagonalForests_Returns3Scores()
        {
            int expectedScore = 3;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Forest, FieldType.Empty }, { FieldType.Forest, FieldType.Empty } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.GreenRegion).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_AllForests_Returns4Scores()
        {
            int expectedScore = 4;
            FieldType[,] fieldTypes = new FieldType[2, 2]
            { { FieldType.Forest, FieldType.Forest }, { FieldType.Forest, FieldType.Forest } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.GreenRegion).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }
    }
}
