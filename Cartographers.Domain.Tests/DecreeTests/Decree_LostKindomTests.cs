﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_LostKindomTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_EmtyFields_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.LostKingdom).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1Area_Returns6Score()
        {
            int expectedScore = 6;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[2, 2] = FieldType.Town;

            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.LostKingdom).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_2EqualSquares_Returns6Scores()
        {
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 1] = FieldType.Town;
            fields[0, 0] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[1, 1] = FieldType.Town;

            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[3, 1] = FieldType.Town;
            fields[3, 2] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.LostKingdom).CountScore(map);
            Assert.AreEqual(6, actualScore);
        }

        [Test]
        public void CountScores_2DiffSquares_Returns6Scores()
        {
            int expectedScore = 6;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 1] = FieldType.Town;
            fields[0, 0] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[1, 1] = FieldType.Town;

            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            fields[3, 1] = FieldType.Town;
            fields[3, 2] = FieldType.Town;
            fields[3, 3] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.LostKingdom).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_2DiffSquares_9Scores()
        {
            int expectedScore = 9;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[0, 1] = FieldType.Town;
            fields[0, 0] = FieldType.Town;
            fields[1, 0] = FieldType.Town;
            fields[1, 1] = FieldType.Town;

            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            fields[3, 1] = FieldType.Town;
            fields[3, 2] = FieldType.Town;
            fields[3, 3] = FieldType.Town;
            fields[4, 1] = FieldType.Town;
            fields[4, 2] = FieldType.Town;
            fields[4, 3] = FieldType.Town;

            Map map = new Map(fields);

            var actualScore = _df.GetByCode(DecreeCode.LostKingdom).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

    }
}
