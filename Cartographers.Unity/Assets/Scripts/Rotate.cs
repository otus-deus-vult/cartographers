﻿using Cartographers.DomainUI;
using System;
using UnityEngine;

namespace Assets.Scripts
{
    class Rotate
    {
        ButtonState _rb = ButtonState.None;
        ButtonState _mb = ButtonState.None;

        public void Action(IClientGameManager gameManager)
        {
            switch (_rb)
            {
             case ButtonState.None:
                if (Input.GetMouseButton(1))
                {
                    gameManager.Rotate();
                    _rb = ButtonState.Pressed;
                }
                break;
             case ButtonState.Pressed:
                if (!Input.GetMouseButton(1))
                    _rb = ButtonState.None;
                break;
            }

            switch (_mb)
            {
                case ButtonState.None:
                    if (Input.GetMouseButton(2))
                    {
                        gameManager.Reflect();
                        _mb = ButtonState.Pressed;
                    }
                    break;
                case ButtonState.Pressed:
                    if (!Input.GetMouseButton(2))
                        _mb = ButtonState.None;
                    break;
            }
        }

      enum ButtonState
      {
         Pressed,
         None
      }
   }
}
