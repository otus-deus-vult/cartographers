﻿using UnityEngine;
using UnityEngine.UI;

class RegistrationMenu : GameMenu
{
    public InputField UserLoginInputField;
    public InputField UserPasswordInputField;
    public string UserUid => UserLoginInputField.text;
    public string UserPassword => UserPasswordInputField.text;
}
