﻿using UnityEngine.UI;

class CreateGameSettigs : GameMenu
{
    public InputField SettingsTurnDurationInputField;
    public int TurnDuration => int.Parse(SettingsTurnDurationInputField.text);
    void Start()
    {
        SettingsTurnDurationInputField.text = "60";
        SettingsTurnDurationInputField.onValidateInput += (string input, int charIndex, char addedChar) => Validate(addedChar);
        SettingsTurnDurationInputField.onEndEdit.AddListener(delegate { CheckValueRange(SettingsTurnDurationInputField); });
    }

    char Validate(char charToValidate)
    {
        if (!char.IsNumber(charToValidate))
        {
            charToValidate = '\0';
        }
        return charToValidate;
    }

    void CheckValueRange(InputField input)
    {
        if (int.Parse(input.text) < 15)
        {
            input.text = "15";
        }
    }
}