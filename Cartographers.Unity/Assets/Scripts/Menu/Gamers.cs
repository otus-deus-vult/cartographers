﻿using Assets.Scripts;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

class Gamers : GameMenu
{
    Dictionary<string, GameObject> _gamers;
    public List<string> _gamerIds;
    List<string> _removeGamerIds;
    bool _active;

    public Gamers()
    {
        _gamers = new Dictionary<string, GameObject>();
        _gamerIds = new List<string>();
        _removeGamerIds = new List<string>();
    }
    public void UpdateGamerList(List<string> gamerIds)
    {
        _gamerIds = gamerIds;
    }

    public void Update()
    {
        if (_gamerIds == null) return;
        foreach (var gamer in _gamerIds)
        {
            if (!_gamers.ContainsKey(gamer))
            {
                int index = _gamers.Count();
                _gamers[gamer] = Utils.CreateCopyByName("GamerListItemOn", "gamer_" + gamer, "GamerList");
                DrawPayerIcon((_gamers.ElementAt(index).Key, _gamers.ElementAt(index).Value), new Vector2(-2.75f, 11f - 1.25f * index));
            }
        }
        foreach (var gamer in _gamers)
        {
            if (!_gamerIds.Contains(gamer.Key))
            {
                GameObject.Destroy(gamer.Value);
                _removeGamerIds.Add(gamer.Key);
            }
        }
        _removeGamerIds.ForEach(f => _gamers.Remove(f));
        _removeGamerIds.Clear();
    }

    public void Destroy() => Utils.DeleteGameObjctsByTag("GamerList");

    public new void Hide()
    {
        _gamers.Select(s => s.Value).ToList().ForEach(f => f.SetActive(false));
        _active = false;
    }
    public new void Show()
    {
        _gamers.Select(s => s.Value).ToList().ForEach(f => f.SetActive(true));
        _active = true;
    }

    void DrawPayerIcon((string gamerId, GameObject obj) gamer, Vector2 position)
    {
        gamer.obj.transform.SetParent(GameObject.Find("MenuCanvas").transform, false);
        gamer.obj.transform.position = position;
        gamer.obj.transform.localScale = new Vector2(1.25f, 1.25f);
        gamer.obj.SetActive(_active);

        GameObject text = gamer.obj.transform.FindChild("GamerName").gameObject;
        text.GetComponent<Text>().text = gamer.gamerId;
        gamer.obj.SetLayer(20);
    }
}