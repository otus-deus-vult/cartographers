﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

class ServerUrlDorpdown : MonoBehaviour
{
    public string ServerUrl;

    void Start()
    {
        var dropdown = transform.GetComponent<Dropdown>();
        dropdown.options.Clear();
        string[] urls =
            {
                "https://localhost:44394/game",
                "http://u1331077.plsk.regruhosting.ru/game"
            };
        dropdown.options.AddRange(urls.ToList().Select(s => new Dropdown.OptionData() { text = s }));
        dropdown.RefreshShownValue();
        dropdown.onValueChanged.AddListener(delegate { DropdownItemSelected(dropdown); });
    }

    void DropdownItemSelected(Dropdown dropdown)
    {
        int index = dropdown.value;
        ServerUrl = dropdown.options[index].text;
    }
}
