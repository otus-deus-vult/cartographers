﻿using UnityEngine;
using UnityEngine.UI;

class LoginMenu : GameMenu
{
    public InputField UserLoginInputField;
    public InputField UserPasswordInputField;
    public string UserUid => UserLoginInputField.text;
    public string UserPassword => UserPasswordInputField.text;

    public GameObject Menu;
    Menu _menu;

    void Start()
    {
        _menu = Menu.GetComponent<Menu>();
        Show();
    }
    public void HideIfLoginNotEmpty()
    {
        if (string.IsNullOrEmpty(UserUid))
        {
            _menu.ShowOnlyThis(this);
        }
        else
        {
            transform.gameObject.SetActive(false);
        }
    }
}
