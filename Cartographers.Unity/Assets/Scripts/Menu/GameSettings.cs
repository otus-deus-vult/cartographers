﻿using UnityEngine;

class GameSettings : GameMenu
{
    public GameObject ServerUrl;
    public string ApiUrl => string.IsNullOrWhiteSpace(ServerUrl.GetComponent<ServerUrlDorpdown>().ServerUrl) ?
        "https://localhost:44394/game" :
        ServerUrl.GetComponent<ServerUrlDorpdown>().ServerUrl;
}