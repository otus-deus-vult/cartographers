﻿using Cartographers.DomainUI;
using UnityEngine.UI;
using UnityEngine;

namespace Assets.Scripts
{
    class TurnTimer : GameMenu
    {
        float _timeLeft;
        float _initialTime;
        bool _isRunning;
        bool _changeColorOverTime = true;
        bool _ignoreNextUpdate;
        float _r = 0.8490566f;
        float _g = 0.6682823f;
        float _b = 0.2763439f;
        Text _timer;

        void Start()
        {
            _timer = GetComponentInChildren<Text>();
        }

        public void UpdateTimer(IClientGameManager gameManager)
        {
            if (!_ignoreNextUpdate)
            {
                Reset();
                _isRunning = !gameManager.GameCancelled && gameManager.TurnAvailable && gameManager.TurnTimeLeft > 0;
                _timeLeft = gameManager.TurnTimeLeft;
                _initialTime = _timeLeft;
                _changeColorOverTime = true;
            }
            _ignoreNextUpdate = false;
        }

        void Update()
        {
            if (_isRunning)
            {
                if (_timeLeft > 0)
                {
                    _timeLeft -= Time.deltaTime;

                    var minutes = (int)(_timeLeft / 60);
                    var seconds = (int)(_timeLeft % 60);
                    float green = _changeColorOverTime ? _g * _timeLeft / _initialTime : _g;
                    _timer.color = new Color(_r, green, _b);
                    _timer.text = string.Format("{0:00}:{1:00}", minutes, seconds);
                }
                else
                {
                    Reset();
                    _timer.text = string.Empty;
                    Hide();
                }
            }
            else
            {
                _timer.text = string.Empty;
            }
        }

        public void Reset()
        {
            _timeLeft = 0;
            _isRunning = false;
            _changeColorOverTime = true;
            _r = 0.8490566f;
            _g = 0.6682823f;
            _b = 0.2763439f;
        }

        public void SetGreenColor()
        {
            _r = 0.04538981f;
            _g = 0.6415094f;
            _b = 0.1103381f;
            _changeColorOverTime = false;
        }

        public void IgnoreNextUpdate()
        {
            _ignoreNextUpdate = true;
        }
    }
}
