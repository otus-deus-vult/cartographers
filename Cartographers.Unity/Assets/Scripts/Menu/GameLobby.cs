﻿using UnityEngine.UI;

public class GameLobby : GameMenu
{
    public InputField GameUidInputField;

    public string GameUid
    {
        get => GameUidInputField.text;
        set => GameUidInputField.text = value;
    }
}
