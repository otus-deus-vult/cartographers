﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

class WaitForResponse : GameMenu
{
    Text _textField;
    string _text;
    Color _textColor = Color.black;
    const float _fadePerFrame = 0.015f;
    float _opacity = 0;
    bool _descendingOpacity = false;
    bool _active = false;
    bool _error = false;
    int _counter = 0;

    public string Text
    {
        get => _text;
        set
        {
            _text = value;
            ResetToDefaults();
        }
    }

    void Awake()
    {
        _textField = GetComponentInChildren<Text>(true);
        ResetToDefaults();
        Utils.SetOpacity(gameObject, 0);
    }
    void Start()
    {
        _textField = GetComponentInChildren<Text>(true);
        ResetToDefaults();
        Utils.SetOpacity(gameObject, 0);
    }

    void FixedUpdate()
    {
        if (!(_textField is null) && _textField.text != _text)
        {
            _textField.text = _text;
            _textField.color = _textColor;
        }
        if (_active)
        {
            SetTransparency();
        }
    }

    void SetTransparency()
    {
        if (_descendingOpacity)
        {
            if (_opacity > 0)
            {
                if (_fadePerFrame < _opacity) _opacity -= _fadePerFrame;
                else { _opacity = 0; _counter++; }

            }
            else
            {
                _descendingOpacity = false;
            }
        }
        else
        {
            if (_opacity < 1)
            {
                if (_fadePerFrame + _opacity < 1) _opacity += _fadePerFrame;
                else {_opacity = 1; _counter++; }
            }
            else
            {
                _descendingOpacity = true;
            }
        }
        if (_error && _counter > 1)
        {
            Hide();
        }
        Utils.SetOpacity(gameObject, _opacity);
    }

    public override void Hide()
    {
        ResetToDefaults();
        _active = false;
        base.Hide();
    }

    public override void Show()
    {
        ResetToDefaults();
        _error = false;
        _active = true;
        _textColor = Color.black;
        base.Show();
    }

    public void Show(string text)
    {
        Text = text;
        Show();
    }

    public void ShowError(string errorMessage)
    {
        Show();
        Text = errorMessage;
        _error = true;
        _textColor = new Color(0.6f, 0, 0);
    }

    void ResetToDefaults()
    {
        _opacity = 0;
        _descendingOpacity = false;
        _counter = 0;
    }
}