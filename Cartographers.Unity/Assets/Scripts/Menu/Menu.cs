﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

class Menu : MonoBehaviour
{
    public GameObject MoveShapes;
    MoveShapes _moveshapes;

    public List<GameObject> MenuElements;

    void Start()
    {
        _moveshapes = MoveShapes.GetComponent<MoveShapes>();
        MenuElements = transform.GetComponentsInChildren<Transform>(true)
                                .Where(w => w.GetComponent<GameMenu>() != null)
                                .Select(s => s.gameObject).ToList();
    }

    public void HideAll() => MenuElements.ToList().ForEach(f => f.SetActive(false));
    public void ShowOnlyThis(GameMenu remainingElement)
    {
        HideAll();
        remainingElement.gameObject.SetActive(true);
    }
    public void Exit()
    {
        _moveshapes.OnDestroy();
        Application.Quit();
    }
}
