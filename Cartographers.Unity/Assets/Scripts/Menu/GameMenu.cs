﻿using UnityEngine;

public class GameMenu : MonoBehaviour
{
    public virtual void Hide() => transform.gameObject.SetActive(false);
    public virtual void Show() => transform.gameObject.SetActive(true);
}
