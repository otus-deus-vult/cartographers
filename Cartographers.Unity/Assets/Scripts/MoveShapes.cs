﻿using Assets.Scripts;
using Cartographers.Domain.DecreeData;
using Cartographers.DomainUI;
using Cartographers.DTO;
using System.Collections.Generic;
using UnityEngine;

public class MoveShapes : MonoBehaviour
{
    public GameObject Menu;
    public GameObject LoginMenu;
    public GameObject RegistrationMenu;
    public GameObject GameLobby;
    public GameObject CreateGameSettings;
    public GameObject GameSettings;
    public GameObject MainMenu;
    public GameObject OpenMenuButton;
    public GameObject Gamers;
    public GameObject Timer;
    public GameObject WaitForResponse;
    public GameObject Score;

    Menu _menu;
    LoginMenu _loginMenu;
    RegistrationMenu _registrationMenu;
    GameLobby _gameLobby;
    CreateGameSettigs _sessionSettings;
    GameSettings _settings;
    MainMenu _mainMenu;
    OpenMenuButton _openMenuButton;
    Gamers _gamers;
    TurnTimer _timer;
    WaitForResponse _waitForResponse;
    ScoreTable _score;

    IClientGameManager _gameManager;
    DraggingObject _drag;
    Rotate _rotate;
    Matrix _board;
    Matrix _figure;
    ResearchCardUI _researchCard;
    Seasons _seasons;
    UnityWorker _worker;

    string _userLogin;
    string _userPassword;

    void Start()
    {
        _menu = Menu.GetComponent<Menu>();
        _loginMenu = LoginMenu.GetComponent<LoginMenu>();
        _registrationMenu = RegistrationMenu.GetComponent<RegistrationMenu>();
        _mainMenu = MainMenu.GetComponent<MainMenu>();
        _gameLobby = GameLobby.GetComponent<GameLobby>();
        _sessionSettings = CreateGameSettings.GetComponent<CreateGameSettigs>();
        _settings = GameSettings.GetComponent<GameSettings>();
        _openMenuButton = OpenMenuButton.GetComponent<OpenMenuButton>();
        _gamers = Gamers.GetComponent<Gamers>();
        _timer = Timer.GetComponent<TurnTimer>();
        _waitForResponse = WaitForResponse.GetComponent<WaitForResponse>();
        _score = Score.GetComponent<ScoreTable>();
        _worker = UnityWorker.Instance;
        _gameManager = new GameManager();
        _gameManager.HideWaitingForResponseMessage += HideWaitingForResponseMessage;
    }

    void GetNewGame()
    {
        Clear();
        _board = new Matrix(Constants.MapSize);
        _figure = new Matrix(Constants.FigureSize);
        _rotate = new Rotate();
        _researchCard = new ResearchCardUI(_gameManager);
        _drag = new DraggingObject();
        _seasons = new Seasons(_gameManager);
        _score.Clear();

        if (_gameManager != null)
        {
            _gameManager.OnMapChangeEvent += UpdateBoard;
            _gameManager.OnFigureChangeEvent += UpdateFigure;
            _gameManager.OnTurnStarted += UpdateTurn;
            _gameManager.OnGameCreated += UpdateGameUid;
            _gameManager.OnGameEnded += ShowScore;
        }
    }

    void UpdateBoard(IClientGameManager gm, MapChangedEventArgs args)
    {
        _board.Update(args.Map, gm.ResearchCard.IsRuin);
        _seasons.Update(gm, args.Map);
        _worker.AddJob(() =>
        {
            _board.Update();
            _seasons.Update();
        });
    }
    void UpdateFigure(IClientGameManager gm, FigureChangedEventArgs args)
    {
        _figure.Update(args.Figure, gm.GameHasEnded);
        _researchCard.Update(gm);
        _worker.AddJob(() =>
        {
            _figure.Update();
            _researchCard.Update();
        });
    }

    void UpdateTurn(IClientGameManager gm)
    {
        _timer.UpdateTimer(gm);
        _worker.AddJob(() =>
        {
            HideMenu();
        });
    }

    void UpdateGameUid(GameCreatedEventArgs args)
    {
        _worker.AddJob(() => { _gameLobby.GameUid = args.Password; });
        _gamers.UpdateGamerList(args.Gamers);
        _worker.AddJob(() => _gamers.Update());
    }

    void HideWaitingForResponseMessage(bool accepted, string message)
    {
        if (accepted)
        {
            _worker.AddJob(() =>
            {
                _waitForResponse.Hide();
                _menu.ShowOnlyThis(_mainMenu);
            });
        }
        else
        {
            _worker.AddJob(() => _waitForResponse.ShowError(message));
        }
    }

    void ShowWaitingForResponseMessage(string message)
        => _waitForResponse.Show(message);

    void ShowScore(List<GamerScore> score, IDecree[] decrees)
    {
        _score.Decrees = decrees;
        _score.Score = score;
        _seasons.Update(score);
        _worker.AddJob(() => _seasons.Update());
        _worker.AddJob(() => _score.Show());
    }

    void InitBackground()
    {
        _researchCard.InitBackground();
        _seasons.Init();
    }

    void Update()
    {
        _drag?.Action(SetFigureOnBoard, _figure, _researchCard);
        _rotate?.Action(_gameManager);
        _researchCard?.ButtonAction();
        _seasons?.ButtonInteract();
        _figure?.SetTransparency(0.075f);
        _researchCard?.SetTransparency(0.075f);
        if (_gameManager != null)
        {
            if (_gameManager.GameCancelled)
            {
                _timer?.Reset();
                _board?.Disintegrate(1);
                _figure?.Disintegrate(1);
                _researchCard?.MarkToDestroy();
                _researchCard?.SetTransparency(0.0125f);
            }
            if (_gameManager.GameHasEnded)
            {
                _timer?.Reset();
            }
        }
    }

    void SetFigureOnBoard(Vector2 from, Vector2 to)
    {
        Point fromPoint = Utils.VectorToPoint(from);
        if (!_gameManager.TurnAvailable || _gameManager.GameHasEnded)
        {
            return;
        }
        fromPoint -= _figure.Offset;
        Point toPoint = Utils.VectorToPoint(to);
        Point movePoint = toPoint - fromPoint;
        if (!_gameManager.CanMove(movePoint))
        {
            return;
        }
        _timer.SetGreenColor();
        _timer.IgnoreNextUpdate();
        _gameManager.Move(movePoint);
    }

    public void HideMenu()
    {
        _mainMenu.Hide();
        _gameLobby.Hide();
        _gamers.Hide();
        _openMenuButton.Show();
        _timer.Show();
    }
    public void ShowMenu()
    {
        _mainMenu.Show();
        _gamers.Show();
        _openMenuButton.Hide();
    }
    public void CreateGameSession()
    {
        GetNewGame();
        _gameManager?.CreateGame(new SessionSettings
        {
            SessionOwner = _loginMenu.UserUid,
            TurnDuration = _sessionSettings.TurnDuration
        });
        InitBackground();
        _gamers.Show();
    }
    public void StartGame()
        => _gameManager.StartGame();

    public void JoinGame()
    {
        GetNewGame();
        _gameManager?.JoinGame(_gameLobby.GameUid);
        InitBackground();
        _gamers.Show();
    }

    public void LoginUser()
    {
        _userLogin = _loginMenu.UserUid;
        _userPassword = _loginMenu.UserPassword;
        if (!string.IsNullOrWhiteSpace(_userLogin) && !string.IsNullOrWhiteSpace(_userPassword))
        {
            _waitForResponse.Show("Ожидание ответа сервера...");
        }
        else
        {
            _waitForResponse.ShowError("Введите логин и пароль...");
            return;
        }
        _gameManager?.Login(_userLogin, _userPassword);
    }
    public void RegisterUser()
    {
        _userLogin = _registrationMenu.UserUid;
        _userPassword = _registrationMenu.UserPassword;
        if (!string.IsNullOrWhiteSpace(_userLogin) && !string.IsNullOrWhiteSpace(_userPassword))
        {
            _waitForResponse.Show("Ожидание ответа сервера...");
        }
        else
        {
            _waitForResponse.ShowError("Введите логин и пароль...");
            return;
        }
        _gameManager?.Register(_userLogin, _userPassword);
    }
    public void Clear()
    {
        _board?.Clear();
        _figure?.Clear();
        _seasons?.Clear();
        _researchCard?.Clear();
        if (_gameManager != null)
        {
            _gameManager.OnMapChangeEvent -= UpdateBoard;
            _gameManager.OnFigureChangeEvent -= UpdateFigure;
            _gameManager.OnTurnStarted -= UpdateTurn;
            _gameManager.OnGameCreated -= UpdateGameUid;
            _gameManager.OnGameEnded -= ShowScore;
            _gameManager.HideWaitingForResponseMessage -= HideWaitingForResponseMessage;
        }
    }

    public void OnDestroy()
    {
        _gameManager.Unsubscribe();
        Clear();
    }
}