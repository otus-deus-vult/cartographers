﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    internal class UnityWorker : MonoBehaviour
    {
        internal static UnityWorker _instance;
        public static object _workerLock = new object();
        Queue<Action> _jobs = new Queue<Action>();

        public static UnityWorker Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }
                lock (_workerLock)
                {
                    if (_instance == null)
                    {
                        _instance = new UnityWorker();
                    }
                    return _instance;
                }
            }
        }

        void Awake()
        {
            lock (_workerLock)
            {
                if (_instance == null)
                {
                    _instance = this;
                    DontDestroyOnLoad(this.gameObject);
                }
            }
        }

        public void Update()
        {
            lock (_workerLock)
            {
                while (_jobs.Count > 0)
                {
                    _jobs?.Dequeue().Invoke();
                }
            }
        }

        internal void AddJob(Action newJob)
        {
            lock (_workerLock)
            {
                _jobs.Enqueue(newJob);
            }
        }

        void OnDestroy()
        {
            _instance = null;
        }
    }
}
