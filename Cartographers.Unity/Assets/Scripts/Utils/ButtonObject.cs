﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class ButtonObject : UIObject
    {
        Vector2 _scale;
        public int ActionIndex = 0;

        public ButtonObject(GameObject go, UnityAction<int> action, string newName)
        {
            UnityAction act = () => action(ActionIndex);
            Object = go;
            Object.name = newName ?? Object.name;
            Object.AddComponent<Button>().onClick.AddListener(act);
            Object.AddComponent<Image>();
            Object.GetComponent<Image>().sprite = Object.GetComponent<SpriteRenderer>().sprite;
            Object.GetComponent<Button>().interactable = true;
            Object.GetComponent<Button>().targetGraphic = Object.GetComponent<Image>();
            Object.AddComponent<Canvas>();
            Object.GetComponent<Canvas>().worldCamera = Camera.main;
        }

        public void SetActionIdex(int index)
        {
            ActionIndex = index;
        }

        public ButtonObject(string objectName, UnityAction<int> action, string newName = null)
            : this(GameObject.Instantiate(GameObject.Find(objectName)), action, newName) { }
    }
}
