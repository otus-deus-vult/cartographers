﻿using Cartographers.DTO;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
   public static class Utils
   {
      public static GameObject CreateCopyByName(Point position, string name, string copyName = null, string tag = null)
      => CreateCopyByName(VectorFromPoint(position), name, copyName, tag);

      public static GameObject CreateCopyByName(Vector2 position, string name, string copyName = null, string tag = null)
      {
         GameObject gameObject = CreateCopyByName(name, copyName, tag);
         gameObject.transform.position = position;
         return gameObject;
      }

        public static GameObject CreateCopyByName(string name, string copyName = null, string tag = null)
        {
            GameObject go;
            if (GameObject.Find(name) != null)
            {
                go = GameObject.Instantiate(GameObject.Find(name));
            }
            else
            {
                go = new GameObject();
                go.name = name;
                go.AddComponent<Canvas>();
            }
            if (copyName != null)
            {
                go.name = copyName;
            }
            if (!string.IsNullOrEmpty(tag))
            {
                go.tag = tag;
            }
            return go;
        }

        public static void DeleteGameObject(string name)
            => DeleteGameObject(GameObject.Find(name));

        public static void DeleteGameObjctsByTag(string tag)
        {
            GameObject.FindGameObjectsWithTag(tag).ToList().ForEach(f => DeleteGameObject(f));
        }

        public static void DeleteGameObject(GameObject gobj)
        {
            if (gobj != null)
            {
                GameObject.Destroy(gobj);
            }
        }

        public static Point VectorToPoint(Vector2 vector)
            => new Point((int)Math.Round(vector.x, 0), (int)Math.Round(vector.y, 0));

        public static Vector2 VectorFromPoint(Point point)
            => new Vector2(point.X, point.Y);

        public static bool IsMouseButtonPressed()
            => Input.GetMouseButton(0);

        public static Transform GetItemAt(Vector2 position)
            => Physics2D.RaycastAll(position, position, 0.5f).FirstOrDefault().transform;

        public static Vector2 GetClickPosition()
            => Camera.main.ScreenToWorldPoint(Input.mousePosition);

        public static void SetOpacity(GameObject go, float opacity)
        {
            if (go.GetComponent<SpriteRenderer>() != null)
            {
                Color color = go.GetComponent<SpriteRenderer>().color;
                Color newColor = new Color(color.r, color.g, color.b, opacity);
                go.GetComponent<SpriteRenderer>().color = newColor;
            }
            if (go.GetComponent<Text>() != null)
            {
                Color color = go.GetComponent<Text>().color;
                Color newColor = new Color(color.r, color.g, color.b, opacity);
                go.GetComponent<Text>().color = newColor;
            }
            if (go.GetComponentInChildren<Text>() != null)
            {
                Color color = go.GetComponentInChildren<Text>().color;
                Color newColor = new Color(color.r, color.g, color.b, opacity);
                go.GetComponentInChildren<Text>().color = newColor;
            }
            if (go.GetComponent<Image>() != null)
            {
                Color color = go.GetComponent<Image>().color;
                Color newColor = new Color(color.r, color.g, color.b, opacity);
                go.GetComponent<Image>().color = newColor;
            }
        }
    }
}
