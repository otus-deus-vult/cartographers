﻿namespace Assets.Scripts
{
   struct Constants
   {
      public const int MapSize = 11;
      public const int FigureSize = 4;
      public const int FigureZeroPositionX = 12;
      public const int FigureZeroPositionY = 6;
   }
}
