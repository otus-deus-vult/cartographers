﻿using System.Threading;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
   class TextObject : UIObject
   {
      public string Name { get; private set; }
      public string Text { get; private set; }

      public TextObject(string name, string text, string newName = null)
      {
         Init(name);
         Name = name;
         Text = text;
         SetDafaultTextParameters();
      }

      public void Init(string name)
      {
         GameObject go = new GameObject();
         go.AddComponent<Text>();
         go.AddComponent<Canvas>();
         go.name = name;
         Object = go;
      }

      void SetDafaultTextParameters()
      {
         if (Object.GetComponent<Text>() == null)
         {
            Object.AddComponent<Text>();
         }
         Object.name = Name;
         Object.GetComponent<Text>().text = Text;
         Object.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
         Object.GetComponent<Text>().color = Color.black;
         Object.GetComponent<Text>().fontSize = 1;
         Object.GetComponent<Text>().font = (Font)Resources.Load("Header", typeof(Font));
         Object.GetComponent<RectTransform>().localScale = new Vector2(1, 1);
         Object.GetComponent<RectTransform>().sizeDelta = new Vector2(10, 1);
         Object.GetComponent<Text>().material = (Material)Resources.Load("DropShadowMaterial", typeof(Material));
      }

      public void SetAlignment(TextAnchor alignment)
      {
         Object.GetComponent<Text>().alignment = alignment;
      }

      public void SetColor(Color color)
      {
         Object.GetComponent<Text>().color = color;
      }

      public void SetFontSize(int fontsize)
      {
         Object.GetComponent<Text>().fontSize = fontsize;
      }

      public void SetFont(string fontName)
      {
         Object.GetComponent<Text>().font = (Font)Resources.Load(fontName, typeof(Font));
      }

      public void CreateTextLabel(Vector2 frame)
      {
         Object.GetComponent<RectTransform>().sizeDelta = frame;
      }

   }
}
