﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class GameObjectExtensions
    {
        public static void SetLayer (this GameObject go, int layer)
        {
            if (go.GetComponent<Canvas>() != null)
                go.GetComponent<Canvas>().sortingOrder = layer;
            if (go.GetComponent<SpriteRenderer>() != null)
                go.GetComponent<SpriteRenderer>().sortingOrder = layer;
        }
    }
}
