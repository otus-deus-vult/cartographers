﻿using UnityEngine;

namespace Assets.Scripts
{
    class DraggingObject
    {
        State _state = State.None;
        Vector2 _offset;
        Vector2 _fromPosition;
        Vector2 _toPosition;
        public delegate void SetFigure(Vector2 from, Vector2 to);

        public void Action(SetFigure setOjbect, Matrix figure, ResearchCardUI researchCard)
        {
            switch (_state)
            {
                case State.None:
                    if (Utils.IsMouseButtonPressed()) { PickUp(figure); }
                    break;
                case State.Drag:
                    if (Utils.IsMouseButtonPressed()) { Drag(figure, researchCard); }
                    else { Drop(setOjbect, figure, researchCard); }
                    break;
            }
        }

        void PickUp(Matrix figure)
        {
            Vector2 clickPosition = Utils.GetClickPosition();
            if (figure.IsEmptyField(clickPosition)) return;
            Transform clickedItem = Utils.GetItemAt(clickPosition);
            _state = State.Drag;
            _fromPosition = clickedItem.position;
            figure.FromPosition = _fromPosition;
            figure.CurrentPosition = clickPosition;
            _offset = _fromPosition - clickPosition;
        }
        void Drag(Matrix figure, ResearchCardUI researchCard)
        {
            researchCard.DisableButtonsWhileDragging = true;
            figure.Position = Utils.GetClickPosition() + _offset;
        }
        void Drop(SetFigure setOjbect, Matrix figure, ResearchCardUI researchCard)
        {
            researchCard.DisableButtonsWhileDragging = false;
            _toPosition = figure.Position;
            setOjbect(_fromPosition, _toPosition);
            _state = State.None;
        }
        enum State
        {
            None,
            Drag
        }
   }
}
