﻿using Cartographers.Domain.ResearchCardData;
using Cartographers.DomainUI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class ResearchCardUI
    {
        public bool DisableButtonsWhileDragging { get; set; }
        bool _clicked = false;
        Vector2 _basePoint = new Vector2(Constants.FigureZeroPositionX + 2, Constants.FigureZeroPositionY);
        const int _territoryButtonOffset = 1;
        const int _shapeButtonOffset = 2;
        bool _gameHasEnded;
        bool _gameCacelled;
        float _opacity = 1.0f;
        bool _markedToDestroy;

        public GameObject Background { get; set; }

        IResearchCard _researchCard;
        Action<int> _changeShape;
        Action<int> _changeType;

        public ResearchCardUI(IResearchCard researchCard, Action<int> changeShape, Action<int> changeType)
        {
            _researchCard = researchCard;
            _changeShape = changeShape;
            _changeType = changeType;
            DisableButtonsWhileDragging = false;
        }

        public ResearchCardUI(IClientGameManager gameManager)
        {
            _researchCard = gameManager.ResearchCard;
            _changeShape = gameManager.ChangeShape;
            _changeType = gameManager.ChangeType;
            DisableButtonsWhileDragging = false;
        }

        public void InitBackground()
        {
            string background = "ShapeBackground";
            if (_researchCard != null && _researchCard.IsRuin)
            {
                background = "RuinShapeCardBackground";
            }
            DeleteBackground();
            Background = Utils.CreateCopyByName(_basePoint, background, "ResearchCardBackground", "ResearchCardBackground");
        }

        public void Update(IClientGameManager gameManager)
        {
            _researchCard = gameManager.ResearchCard;
            _changeShape = gameManager.ChangeShape;
            _changeType = gameManager.ChangeType;
            _gameHasEnded = gameManager.GameHasEnded;
            _gameCacelled = gameManager.GameCancelled;
            if (!gameManager.GameCancelled && !_gameHasEnded)
            {
                _markedToDestroy = false;
            }
        }

        public void Update()
        {
            DeleteButtons();
            DeleteResearchCardHeaders();
            if (_gameHasEnded || _gameCacelled)
            {
                DeleteBackground();
            }
            else
            {
                CreateButtons();
                CreateResearchCardHeaders();
                InitBackground();
            }
        }

        public void CreateButtons()
        {
            int fieldTypesCount = _researchCard.Fields.Length;
            if (fieldTypesCount > 1)
            {
                float startPosition = _basePoint.x - 1.5f;
                int offset = _shapeButtonOffset;
                if (fieldTypesCount > 2)
                {
                    startPosition = _basePoint.x - 2.5f;
                    offset = _territoryButtonOffset;
                }
                for (int i = 0; i < _researchCard.Fields.Length; i++)
                {
                    ButtonObject bo = new ButtonObject
                        (
                            objectName: _researchCard.Fields[i].ToString(),
                            action: (int index) =>
                            {
                                if (!_clicked)
                                {
                                    _clicked = true;
                                    _changeType(index);
                                }
                            },
                            newName: $"btn_territory_{i}"
                        );
                    bo.SetActionIdex(i);
                    bo.SetPosition(new Vector2(startPosition + offset * i, 4));
                    bo.SetTag("btn");
                }
            }

            int j = 0;
            if(_researchCard.Shapes.Length > 1)
            {
                foreach (Shape shape in _researchCard.Shapes)
                {
                    ButtonObject bo = new ButtonObject
                        (
                            objectName: shape.ToString() + (shape.HasGoldCoin ? "_coin" : ""),
                            action: (int index) =>
                            {
                                if (!_clicked)
                                {
                                    _clicked = true;
                                    _changeShape(index);
                                }
                            },
                            newName: $"btn_shape_{j}"
                        );
                    bo.SetActionIdex(j);
                    bo.SetPosition(new Vector2(_basePoint.x - 1.5f + _shapeButtonOffset * j++, 4));
                    bo.SetTag("btn");
                }
            }
        }

        public void ButtonAction()
        {
            if (!DisableButtonsWhileDragging && Input.GetMouseButton(0))
            {
                Vector2 clickPosition = Utils.GetClickPosition();
                Transform clickedItem = Utils.GetItemAt(clickPosition);
                if (clickedItem != null && clickedItem.gameObject.tag == "btn")
                {
                    clickedItem.gameObject.GetComponent<Button>().onClick.Invoke();
                }
            }
            if (!Input.GetMouseButton(0))
            {
                _clicked = false;
            }
        }

        void CreateResearchCardHeaders()
        {
            TextObject researchCardName = new TextObject("ResearchCardHeader", $"{_researchCard.Name}");
            researchCardName.SetPosition(new Vector2(_basePoint.x - 0.4f, 10));
            researchCardName.SetFrame(new Vector2(10, 1.25f));
            researchCardName.SetScale(new Vector2(0.5f, 0.5f));
            researchCardName.SetLayer(2);
            researchCardName.SetColor(Color.black);
            researchCardName.SetTag("ResearchCardHeaders");

            TextObject researchCardMovePoints = new TextObject("ResearchCardMovePoints", $"{_researchCard.MovePoints}");
            researchCardMovePoints.SetPosition(new Vector2(_basePoint.x + 1.75f, 10));
            researchCardMovePoints.SetFrame(new Vector2(1.25f, 1.25f));
            researchCardMovePoints.SetScale(new Vector2(0.5f, 0.5f));
            researchCardMovePoints.SetLayer(2);
            researchCardMovePoints.SetColor(Color.black);
            researchCardMovePoints.SetTag("ResearchCardHeaders");
        }

        void DeleteButtons() => Utils.DeleteGameObjctsByTag("btn");
        void DeleteResearchCardHeaders() => Utils.DeleteGameObjctsByTag("ResearchCardHeaders");
        void DeleteBackground() => Utils.DeleteGameObjctsByTag("ResearchCardBackground");

        public void Clear()
        {
            DeleteButtons();
            DeleteResearchCardHeaders();
            DeleteBackground();
        }

        public void SetTransparency(float amountPerFrame)
        {
            if (_markedToDestroy)
            {
                if (_opacity > 0)
                {
                    if (amountPerFrame < _opacity) _opacity -= amountPerFrame;
                    else _opacity = 0;
                }
            }
            else
            {
                if (_opacity < 1)
                {
                    if (amountPerFrame + _opacity < 1) _opacity += amountPerFrame;
                    else _opacity = 1;
                }
            }
            List<GameObject> disintegreatingItems = new List<GameObject>();
            disintegreatingItems.AddRange(GameObject.FindGameObjectsWithTag("btn"));
            if (_markedToDestroy)
            {
                disintegreatingItems.AddRange(GameObject.FindGameObjectsWithTag("ResearchCardBackground"));
            }
            disintegreatingItems.AddRange(GameObject.FindGameObjectsWithTag("ResearchCardHeaders"));
            disintegreatingItems.ForEach(f => Utils.SetOpacity(f, _opacity));
        }
        public void MarkToDestroy() => _markedToDestroy = true;
    }
}
