﻿namespace Cartographers.DbApi.Data
{
    public class Config
    {
        public static string ConnectionString { get; set; }
        public static bool UseSqlLite { get; set; }
    }
}
