﻿using Cartographers.DTO.Db;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cartographers.DbApi.Data
{
    public interface IDbContext
    {
        DbSet<Gamer> Gamers { get; set; }
        DbSet<Game> Games { get; set; }
        DbSet<Session> Sessions { get; set; }
        DbSet<Status> Statuses { get; set; }
        Task<int> SaveChangesAsync();
        Task AddGamerAsync(Gamer gamer);
        Task AddGamesAsync(IEnumerable<Game> games);
        Task<Gamer> GetGamerAsync(string gamerLogin);
        Task<Session> GetSessionAsync(string sessionUid);
        Task<List<Game>> GetGamesAsync(string sessionUid);
    }
}
