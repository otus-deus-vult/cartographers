﻿using Cartographers.DTO.Db;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cartographers.DbApi.Data
{
    public class AppDbContext : DbContext, IDbContext
    {
        public DbSet<Gamer> Gamers { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Game> Games { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.EnsureEntitiesConfigured();
            base.OnModelCreating(modelBuilder);
        }

        public async Task<int> SaveChangesAsync()
            => await base.SaveChangesAsync()
                         .ConfigureAwait(false);

        public async Task AddGamerAsync(Gamer gamer)
        {
            Gamers.Add(gamer);
            await SaveChangesAsync();
        }

        public async Task<Gamer> GetGamerAsync(string gamerLogin)
            => await Gamers.FirstOrDefaultAsync(f => f.Login == gamerLogin)
                           .ConfigureAwait(false);

        public async Task AddGamesAsync(IEnumerable<Game> games)
        {
            Games.AddRange(games);
            await SaveChangesAsync();
        }

        public async Task<Session> GetSessionAsync(string sessionUid)
            => await Sessions.FirstOrDefaultAsync(f => f.Uid == sessionUid);

        public async Task<List<Game>> GetGamesAsync(string sessionUid)
            => await Games.Where(w => w.SessionUid == sessionUid)
                          .ToListAsync()
                          .ConfigureAwait(false);
    }
}
