﻿using Cartographers.Api.SessionManager;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CartographersApi.Controllers
{
    [ApiController]
	[Route("[controller]")]
	public class GameController : ControllerBase
	{
		private SessionsManager _sm;

		public GameController(SessionsManager sm)
		{
			_sm = sm;
		}

		[HttpGet]
		[Route("getallgames")]
		public Dictionary<string, List<string>> GetAllGames() => _sm.GetGames();
	}
}
