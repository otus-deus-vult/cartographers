﻿using Cartographers.Domain;
using Cartographers.Domain.DecreeData;
using Cartographers.Domain.ResearchCardData;
using Cartographers.Domain.SeasonData;
using Cartographers.DTO;
using Cartographers.Domain.Game;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace Cartographers.Api.SessionManager
{
    public class Session: IDisposable
	{
		private int TurnDuration = 60;

		private readonly CardDeckManager _cdm;
		private readonly ResearchCardFactory _rcf;
		private readonly DecreeFactory _df;

		internal string _sessionOwner;
		internal string _sessionUid;
		internal ConcurrentDictionary<string, ApiGame> _games = new();
		private ConcurrentDictionary<string, string> _mapsExchange = new();

		private CardDeck _deck;
		private IResearchCard _currentCard;
		private BoardDTO _baseBoard;
		private IDecree[] _decrees;

		private Season _season = SeasonProvider.GetSeasonByCode(SeasonCode.Spring);
		private int _seasonTurn = -1;

		internal bool _sessionStarted = false;
		private bool _sessionEnded = false;
		private DateTime _turnEndTime;

		private Timer _turnTimer;

		public List<string> Gamers => _games.Select(s => s.Key).ToList();

		internal event Action<Session> OnTurnStarted;
		internal event Action<Session> OnGameEnded;

		public Session(ResearchCardFactory rcf, DecreeFactory df, SessionSettings settings)
		{
			_cdm = new CardDeckManager(rcf);
			_deck = _cdm.CreateNextCardDeck(_season);
			_baseBoard = GetInitBoardDTO();
			_decrees = df.GetDecrees();
			_rcf = rcf;
			_df = df;
			_sessionOwner = settings.SessionOwner;
			_sessionUid = settings.SessionUid;
			TurnDuration = settings.TurnDuration;
		}

		public ApiGame GetGame(string gamerId)
		{
			if (!_games.ContainsKey(gamerId))
				return null;
			return _games[gamerId];
		}

		public ApiGame CreateGame(string gamerId)
		{
			if (_sessionStarted)
				return null;
			if (_games.ContainsKey(gamerId))
			{
				var game = _games[gamerId];
				return game;
			}
			else
			{
				var gameDTO = new GameDTO()
				{
					Board = _baseBoard,
					SeasonTurn = 0,
					DecreeIds = _decrees.Select(d => (int)d.Code).ToArray(),
					ResearchCardId = _currentCard == null ? 1 : (int)_currentCard.Code,
					Score = 0,
					SeasonId = 0,
					Coins = 0
				};
				var game = new ApiGame(gameDTO, _rcf, _df);
				game.GamerScore.GamerLogin = gamerId;
				_games.TryAdd(gamerId, game);
			}
			return _games[gamerId];
		}

		public void MakeMove(string gamerId, MoveDTO move)
		{
			if (!_games.ContainsKey(gamerId) || !_sessionStarted)
				return;

			if (!_games[gamerId].TurnAvailable || _games[gamerId].GameHasEnded)
				return;

			if (_games[gamerId].TurnAvailable)
			{
				if (!_games[gamerId].Move(move))
					return;
				_games[gamerId].TurnAvailable = false;
				if (_games.Where(w => !w.Value.GameCancelled).All(a => !a.Value.TurnAvailable))
				{
					StartNextTurn();
				}
			}
		}

		public void StartSession()
		{
			_sessionStarted = true;
			_turnTimer = new Timer(TurnDuration * 1000);
			_turnTimer.Elapsed += OnTimerElapsed;
			_turnTimer.Enabled = true;
			StartNextTurn();
		}

		private void OnTimerElapsed(object source, ElapsedEventArgs args)
		{
			StartNextTurn();
		}

		private void StartNextTurn()
		{
			SetNewTurnEndTime(DateTime.Now);
			_games.Select(s => s.Value)
					.Where(w => w.TurnAvailable)
					.ToList()
					.ForEach(g => g.GameCancelled = true);
			_seasonTurn += _currentCard?.MovePoints ?? 1;
			if (_season.IsEnded(_seasonTurn))
			{
				_games.Select(s => s.Value).ToList().ForEach(g => g.CountScore());
				if (_season.IsEndSeason())
				{
					_sessionEnded = true;
					_turnTimer.Dispose();
					OnGameEnded.Invoke(this);
					return;
				}
				_seasonTurn = 0;
				_season = SeasonProvider.GetSeasonByIndex((int) _season.Code + 1);
				_deck = _cdm.CreateNextCardDeck(_season, _deck);
			}
			_currentCard = _deck.GetCard();
			_games.Select(s => s.Value)
					.Where(w => !w.GameCancelled)
					.ToList()
					.ForEach(g => g.NextTurn(_season, _seasonTurn, _currentCard, _turnEndTime));
			ExchangeMaps(_currentCard.IsMonster, _mapsExchange.Count == 0);
			OnTurnStarted?.Invoke(this);
		}

		private void ExchangeMaps(bool isMonster, bool mappingEmpty)
		{
			if (isMonster && _games.Count(c => !c.Value.GameCancelled) > 1 && mappingEmpty)
			{
				var maps = _games.Select(s => new { gamerId = s.Key, map = s.Value.Map })
									.ToDictionary(d => d.gamerId, d => d.map);
				foreach (var game in _games)
				{
					var mappedGamer = _games.Select(s => s.Key)
											.Where(w => !_mapsExchange.Select(s => s.Value).Any(a => a == w) && w != game.Key)
											.Shuffle()
											.First();
					_mapsExchange[game.Key] = mappedGamer;
					_games[game.Key].SetMap(maps[mappedGamer]);
				}
			}
			if (!isMonster && !mappingEmpty)
			{
				var maps = _games.Select(s => new { gamerId = s.Key, map = s.Value.Map })
					.ToDictionary(d => d.gamerId, d => d.map);
				foreach (var game in _games)
				{
					_games[game.Key].SetMap(maps[_mapsExchange[game.Key]]);
					_mapsExchange.TryRemove(game.Key, out var val);
				}
			}
		}

		private void SetNewTurnEndTime(DateTime turnStart)
		{
			if (_sessionEnded)
				return;
			_turnEndTime = turnStart.AddSeconds(TurnDuration);
			_turnTimer.Stop();
			_turnTimer.Start();
		}

		private BoardDTO GetInitBoardDTO()
		{
			var board = new BoardDTO
			{
				Width = 11,
				Height = 11,
				Fields = new FieldType[11 * 11]
			};
			board.Fields[13] = FieldType.Ruin;
			board.Fields[18] = FieldType.Ruin;
			board.Fields[56] = FieldType.Ruin;
			board.Fields[64] = FieldType.Ruin;
			board.Fields[101] = FieldType.Ruin;
			board.Fields[107] = FieldType.Ruin;
			board.Fields[24] = FieldType.Mountain;
			board.Fields[42] = FieldType.Mountain;
			board.Fields[60] = FieldType.Mountain;
			board.Fields[78] = FieldType.Mountain;
			board.Fields[96] = FieldType.Mountain;
			return board;
		}

		public void Dispose()
		{
			_turnTimer?.Dispose();
		}
	}
}
