﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.ResearchCardData;
using Cartographers.DTO;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Cartographers.Api.SessionManager
{
    public class SessionsManager
	{
		private readonly ResearchCardFactory _rcf;
		private readonly DecreeFactory _df;
		private ConcurrentDictionary<string, Session> _sessions = new();

		public SessionsManager(ResearchCardFactory rcf, DecreeFactory df)
		{
			_rcf = rcf;
			_df = df;
		}

		public Session CreateSession(SessionSettings settings)
		{
			var sessionId = GeneratePassword();
			settings.SessionUid = sessionId;
			Session session = new(_rcf, _df, settings);
			_sessions.TryAdd(sessionId, session);
			return session;
		}

		public Session GetSession(string sessionId)
		{
			if (!_sessions.ContainsKey(sessionId))
				return null;
			return _sessions[sessionId];
		}

		private string GeneratePassword()
		{
			return Guid.NewGuid().ToString();
		}

		public Dictionary<string, List<string>> GetGames()
		{
			return _sessions.Select(s => (s.Key, s.Value.Gamers)).ToDictionary(d => d.Key, d => d.Gamers);
		}
	}
}
