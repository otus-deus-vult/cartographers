using Cartographers.Api;
using Cartographers.Api.SessionManager;
using Cartographers.DTO;
using Cartographers.Domain.ResearchCardData;
using Cartographers.Rabbit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using Cartographers.Domain.DecreeData;

namespace CartographersApi
{
    public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<ResearchCardFactory>();
			services.AddSingleton<DecreeFactory>();
			services.AddSingleton<SessionsManager>();
			services.AddControllers();
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "CartographersApi", Version = "v1" });
			});
			var settings = Settings.GetDefault();
			services.AddSingleton<IQueue>(new Queue(Settings.GetDefault()));
			services.AddHostedService<GameService>();
			services.AddSingleton(new List<AuthenticationDTO>());
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{

			if (true)
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CartographersApi v1"));
			}

			if (env.IsDevelopment())
			{
				app.UseHttpsRedirection();
			}

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
