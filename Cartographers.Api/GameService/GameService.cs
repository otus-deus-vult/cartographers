﻿using Cartographers.Api.SessionManager;
using Cartographers.DTO;
using Cartographers.Rabbit;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cartographers.Api
{
    public class GameService : IHostedService
    {
        private SessionsManager _sm;
        private IQueue _queue;
        private List<AuthenticationDTO> _gamers;
        public GameService(SessionsManager sm, IQueue queue, List<AuthenticationDTO> gamers)
        {
            _sm = sm;
            _queue = queue;
            _gamers = gamers;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _queue.Subscribe<AuthenticationDTO>(dto      => AddGamer(dto),        "GameApi",            "DbApi");
            _queue.Subscribe<SessionSettings>  (settings => CreateGame(settings), "GameApi.CreateGame", "GameApi.CreateGame");
            _queue.Subscribe<GamerInfo>        (info     => JoinGame(info),       "GameApi.JoinGame",   "GameApi.JoinGame");
            _queue.Subscribe<MoveRequest>      (request  => MakeMove(request),    "GameApi.MakeMove",   "GameApi.MakeMove");
            _queue.Subscribe<GamerInfo>        (info     => StartGame(info),      "GameApi.StartGame",  "GameApi.StartGame");
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        void CreateGame(SessionSettings settings)
        {
            if (!Authorised(settings.SessionOwner))
                return;
            var session = _sm.CreateSession(settings);
            var info = new GamerInfo { GamerLogin = settings.SessionOwner, SessionId = session._sessionUid};
            session.OnTurnStarted += SendGamesToGamers;
            session.OnGameEnded += SendScoreToGamers;
            session.OnGameEnded += SendScoreToDb;
            _queue.Publish(GetNewGame(info), settings.SessionOwner + ".CreateGame");
        }

        void JoinGame(GamerInfo info)
        {
            if (!Authorised(info.GamerLogin))
                return;
            GetNewGame(info);
        }

        void MakeMove(MoveRequest req)
        {
            if (!Authorised(req.GamerLogin))
                return;
            var session = _sm.GetSession(req.SessionId);
            if (session != null)
            {
                session.MakeMove(req.GamerLogin, req.Move);
            }
        }

        void StartGame(GamerInfo info)
        {
            if (!Authorised(info.GamerLogin))
                return;
            var session = _sm.GetSession(info.SessionId);
            if (session._sessionOwner == info.GamerLogin)
            {
                session.StartSession();
            }
        }

        void SendGamesToGamers(Session session)
        {
            var games = session._games;
            games.Where(w => !w.Value.GameHasEnded)
                 .Select(s => s.Key)
                 .ToList()
                 .ForEach(g => _queue.Publish(games[g].GetGameDTO(), g + ".GetGame"));
            if (games.All(a => a.Value.GameHasEnded || a.Value.GameCancelled))
            {
                session.OnTurnStarted -= SendGamesToGamers;
            }
            var leavers = games.Where(w => w.Value.GameCancelled)
                               .ToList();
            if (leavers.Any())
            {
                var scores = session._games.Where(w => leavers.Contains(w))
                           .Select(s => s.Value.GamerScore)
                           .ToList();
                scores.ForEach(f => { f.SessionUid = session._sessionUid; f.Status = "Canceled"; });
                _queue.Publish(scores, "DbApi.SaveGameResult");
                scores.ForEach(f => _queue.Publish(scores, f.GamerLogin + ".ShowScore"));
                leavers.ForEach(f => games.TryRemove(f));
            }
        }

        void SendScoreToGamers(Session session)
        {
            var scores = session._games.Select(s => s.Value.GamerScore).ToList();
            scores.ForEach(f => _queue.Publish(scores, f.GamerLogin + ".ShowScore"));
            session.OnGameEnded -= SendScoreToGamers;
        }

        void SendScoreToDb(Session session)
        {
            var scores = session._games.Select(s => s.Value.GamerScore).ToList();
            scores.ForEach(f => { f.SessionUid = session._sessionUid; f.Status = "Finished"; });
            _queue.Publish(scores, "DbApi.SaveGameResult");
            session.OnGameEnded -= SendScoreToDb;
        }

        void UpdateGamerList(Session session)
        {
            session.Gamers.ForEach(f => _queue.Publish(session.Gamers, f + ".GetGamers"));
        }

        GameWithGamerInfo GetNewGame(GamerInfo info)
        {
            var session = _sm.GetSession(info.SessionId);
            if (session == null)
                return null;
            var game = session.GetGame(info.GamerLogin) ?? session.CreateGame(info.GamerLogin);
            UpdateGamerList(session);
            return new GameWithGamerInfo()
            {
                Game = game?.GetGameDTO(),
                GamerLogin = info.GamerLogin,
                SessionId = info.SessionId,
            };
        }

        void AddGamer(AuthenticationDTO dto)
        {
            if (_gamers.Any(a => a.Gamer.Login == dto.Gamer.Login))
            {
                var index = _gamers.Select((s, index) => new { Login = s.Gamer.Login, Index = index })
                                   .First(f => f.Login == dto.Gamer.Login)
                                   .Index;
                _gamers.RemoveAt(index);
            }
            _gamers.Add(dto);
        }

        bool Authorised(string gamerId) => _gamers.Any(a => a.Gamer.Login == gamerId && a.Accepted);
    }
}
