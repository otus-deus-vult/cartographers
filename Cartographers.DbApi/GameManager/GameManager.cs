﻿using Cartographers.DbApi.Data;
using Cartographers.DTO;
using Cartographers.Rabbit;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Cartographers.DbApi
{
    public class GameManager : IHostedService
    {
        private readonly IQueue _queue;
        private readonly IDbManager _manager;

        public GameManager(IQueue queue, IDbManager manager)
        {
            _queue = queue;
            _manager = manager;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _queue.Subscribe<IEnumerable<GamerScore>>(async score => await _manager.SetGameScoreAsync(score), "DbApi.SaveGameResult", "DbApi.SaveGameResult");
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
