﻿using Cartographers.DbApi.Data;
using Cartographers.DTO.Db;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Cartographers.DbApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class GamersController : ControllerBase
    {
        private IDbContext dbContext;
        public GamersController(IDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Get all gamers from database
        /// </summary>
        [HttpGet("getgamers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<Gamer> GetGamers()
            => dbContext.Gamers;
    }
}
