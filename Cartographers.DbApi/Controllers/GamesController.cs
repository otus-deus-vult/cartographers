﻿using Cartographers.DbApi.Data;
using Cartographers.DTO.Db;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Cartographers.DbApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private IDbContext dbContext;
        public GamesController(IDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Get all games from database
        /// </summary>
        [HttpGet("getgames")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<Game> GetGames()
            => dbContext.Games;
    }
}
