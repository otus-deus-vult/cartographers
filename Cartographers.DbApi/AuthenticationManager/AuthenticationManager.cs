﻿using Cartographers.DbApi.Data;
using Cartographers.DTO;
using Cartographers.DTO.Db;
using Cartographers.Rabbit;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Cartographers.DbApi
{
    public class AuthenticationManager : IHostedService
    {
        private readonly IQueue _queue;
        private readonly IDbManager _manager;
        public AuthenticationManager(IQueue queue, IDbManager manager)
        {
            _queue = queue;
            _manager = manager;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _queue.Subscribe<Gamer>(async gamer => await ProcessGamer(gamer, _manager.RegisterAsync), "DbApi.Register", "DbApi.Register");
            _queue.Subscribe<Gamer>(async gamer => await ProcessGamer (gamer, _manager.LoginAsync), "DbApi.Login", "DbApi.Login");
            return Task.CompletedTask;
        }

        async Task ProcessGamer(Gamer gamer, Func<Gamer, Task<AuthenticationDTO>> action)
        {
            var result = await action.Invoke(gamer);
            if (result.Accepted)
                _queue.Publish(result, "DbApi");
            _queue.Publish(result, result.Gamer.Login + ".Db");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
