﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.ResearchCardData;
using Cartographers.Domain.SeasonData;
using Cartographers.DTO;
using System;
using System.Collections.Generic;

namespace Cartographers.DomainUI
{
    public class FigureChangedEventArgs : EventArgs
	{
		public FieldType[,] Figure;
	}

	public class MapChangedEventArgs : EventArgs
	{
		public FieldType[,] Map;
	}

	public class SurfacesChangedEventArgs : EventArgs
	{
		public FieldType[,] Figure;
		public FieldType[,] Map;
	}

	public class GameCreatedEventArgs : EventArgs
	{
		public string Password;
		public List<string> Gamers;
	}

	public delegate void FigureChanged(IClientGameManager gm, FigureChangedEventArgs args);
	public delegate void MapChanged(IClientGameManager gm, MapChangedEventArgs args);
	public delegate void TurnStarted(IClientGameManager gm);
	public delegate void GameCreated(GameCreatedEventArgs args);

    public interface IClientGameManager
	{
		int SeasonTurn { get; }
		Season Season { get; }
		IDecree[] Decrees { get; }
		IResearchCard ResearchCard { get; }
		string SessionId { get; set; }
		string GamerLogin { get; set; }
		bool GameHasEnded { get; }
		bool GameCancelled { get; }
		bool TurnAvailable { get; }
		int TurnTimeLeft { get; }
		int Coins { get; }
		int Score { get; }
		int Monsters { get; }
		void InitEmpty();
		void Register(string gamerName, string gamerPassword);
		void Login(string gamerName, string gamerPassword);
		void CreateGame(SessionSettings settings);
		void JoinGame(string sesionId);
		void StartGame();
		void Move(Point position);
		bool CanMove(Point position);
		void Rotate();
		void Reflect();
		void ChangeShape(int index);
		void ChangeType(int index);

		void Unsubscribe();

		event FigureChanged OnFigureChangeEvent;
		event MapChanged OnMapChangeEvent;
		event TurnStarted OnTurnStarted;
		event GameCreated OnGameCreated;
		event Action<bool, string> HideWaitingForResponseMessage;
		event Action<List<GamerScore>, IDecree[]> OnGameEnded;
	}
}
