1. Игрок запускает один из игровых клиентов(это может быть десктопное приложение, веб-страница или мобильное приложение) и "попадает" в свой игровой профиль.

2. В своем профиле клиента игрок инициирует игровую сессию ([Game](../../CRC/Game.md))
   
   2.1 Игрок может присоеденится в уже созданную игровую сессию по приглашению
   
3. В игровой сессии игрок видет игровое поле, панели с карточками и указами, соперников, а так же окно чата с соперниками.

4. Игрок видит активную карту исследований поверх уже выбранных
   4.1 Если выпадает карта "Руины", то автоматически выбирается следующая за ней карта.
   4.2 Если выпадает карта "Засада" - происходит обмен игровыми планшетами
   
5. Игрок пытается разместить ее на игровом поле - трансформируя(поворачивая, перемещая, отзекраливая) контур.
   5.1 Если предыдущая карта была карта руин, то игрок обязан разместить контур используя клетку руин.
   5.2 Если текущая карта "Засада", то игрок рисует(выставляет) заданный контур своему сопернику.
   5.3 Карты монстров могут ограничивать трансформацию контуров фигуры - (запрещать поворот)

6. Игроки одновременно выставляют фигуры исследований на игровом поле (планшете)
   6.1 Если игрок не может выставить фигуру он выбирет произвольную клетку на поле, которая закрашиваеться выбранным типом местности.
   6.2 Если текущая карта "Засада", то контур фигуры заполняется типом местности "монстры"
7. Фигуры не могут перекрывать уже установленные
8. Фигуры не могут выходить за рамки игрового поля
9.  Фигуры не могут накладываться на горы и "пропасти"
10. Смена сезонов происходит автоматически при достижении количества игровых недель.
11.  При достижении (больше или равно) количества игровых недель - происходит остановка раунда, смена сезона и подсчет игровых очков.
   11.1 Подсчет игровых очком
   11.2 Подсчет монеток.
   11.3 Посчет дополнительных очков 
      - каждая монетка в конце каждого сезона приносит по дополнительному очку
      - каждая клетка монстров примыкающими пустыми клетками отнимают по 1му очку
   
12.  