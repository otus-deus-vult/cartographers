Class: Card

Responsibility: Класс игровой карточки. Общие для всех карт свойства и поведение.

Collaboration: [CardType](./CardType.md), [Rule](./Rule.md)