# Domain

- Game
  - Map map
  - int Score
  - Figure currentFigure
  - Season currentSeason
  - ResearchCard currentResearchCard
  - bool CanMove(Vector2<int> position)
  - void Rotate/ReflectFigure()
- Map
  - Map(Board board)
  - Board board
  - bool CanAdd(Figure fig, Vector2<int> position)
- Figure
  - Board board
  - void Rotate()
  - void Reflect()
- Board
  - int width
  - int height
  - FieldType fields[]
  - FieldType this[int, int]
- static Figure/Season/ResearchCardAccessor
  - getById(int id)
  - getList()

# Client

- ClientGame: Game
  - private ApiClient client
  - int gameId
  - int gamerId
  - async Task Init()
  - void InitEmpty
  - async Task Move(Vector2<int> Position)
  


# Interfaces
- Shape
  - bool[,] contour
  - bool HasGoldCoin

- Position
  - int x
  - int y

- IResearchCard
  - Shape[] shapes
  - FieldType[] fieldTypes
  - string CardName
  - int MoveCount

- IClientGame
  - int SeasonTurn { get; }
  - Season Season { get; }
  - IResearchCard ResearchCard {get; }
  - string Password { get; } // создается после вызова CreateGame
  - void InitEmpty;
  - async Task CreateGame();
  - async Task JoinGame(string password);
  - async Task Move(Position position);
  - async Task<bool> NewTurnAvaible();
  - void Rotate();
  - void Reflect();
  - void ChangeShape(int index);
  - void ChangeType(int index);
  - Decree[2] GetDecrees(SeasonCode code);
  - Season[4] GetSeasons();
  - event OnFigureChangeEvent(this, figureeventargs);
  - event OnMapChangeEvent(this, eventargs);

figureeventargs
{
  FieldType[,] Figure { get; }
}

mapeventargs
{
  FieldType[,] Map { get; }
}
  
onChangeEvent += (this) => {
    var figure = this.Figure;
    redraw(figure);
}

# DTO

- GameDTO

  - Board board
  - int score
  - int figureId
  - int seasonId
  - int[4] decreeId
  - int researchCardId
  - bool gameEnded
  - bool isAnomaly

- MoveDTO
  - int figureId
  - ...
  - Vector2<int> position

# Controllers

- {game: GameDTO, gameId: int, gamerId: int} CreateGame(string password)
- {game: GameDTO, gameId: int, gamerId: int} JoinGame(string password)
- GameDTO MakeMove(int gameId, MoveDTO move)

# API

- APIGame: Game

  - void MakeMove(Vector2<int> Position)
  - GameDTO GetGameDTO()

- Session

  - Dictionary<int gamerId,APIGame> games
  - GetGame(int gamerId)
  - int Join()

- GamesManager
  - Dictionary<int id, Session> sessions
  - Dictionary<string password, Session> sessionsByPassword
  - int CreateSession()
  - APIGame GetSession(int id)
  - APIGame GetSession(string password)
