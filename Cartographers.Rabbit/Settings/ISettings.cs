﻿using RabbitMQ.Client;

namespace Cartographers.Rabbit
{
    public interface ISettings
    {
        string Exchange { get; }
        IModel Model { get; }
    }
}
