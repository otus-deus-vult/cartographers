﻿using RabbitMQ.Client;
using System;

namespace Cartographers.Rabbit
{
    public class Settings : ISettings
    {
        private readonly string _exchange;
        private readonly IModel _model;
        public string Exchange => _exchange;
        public IModel Model => _model;

        private Settings(string exchange, IModel model)
        {
            _exchange = exchange;
            _model = model;
        }

        public class Builder
        {
            private string _user;
            private string _password;
            private string _host;
            private int _port;
            private string _exchange;
            private string _uri;

            public Builder SetUser(string user)
            {
                _user = user;
                return this;
            }
            public Builder SetPassword(string password)
            {
                _password = password;
                return this;
            }
            public Builder SetHost(string host)
            {
                _host = host;
                return this;
            }
            public Builder SetPort(int port)
            {
                _port = port;
                return this;
            }
            public Builder SetExchange(string exchange)
            {
                _exchange = exchange;
                return this;
            }
            public Builder SetUri(string uri)
            {
                _uri = uri;
                return this;
            }
            public Settings Build()
            {
                var connection = _uri is null ?
                    new ConnectionFactory
                    {
                        UserName = _user,
                        Password = _password,
                        HostName = _host,
                        Port = _port,
                        DispatchConsumersAsync = true
                    } :
                    new ConnectionFactory
                    {
                        Uri = new Uri(_uri),
                        DispatchConsumersAsync = true
                    };
                IModel model = connection.CreateConnection().CreateModel();
                return new Settings(_exchange, model);
            }
        }
        public static ISettings GetDefault()
            => new Builder()
                .SetUri("amqp://guest:guest@rabbit")
                .SetExchange("cartographers_exchange")
                .Build();
    }
}
