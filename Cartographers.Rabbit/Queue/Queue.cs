﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cartographers.Rabbit
{
    public class Queue : IQueue
    {
        private readonly IModel _model;
        private readonly string _exchange;
        private readonly Dictionary<string, object> _properties = new Dictionary<string, object>() { { "x-message-ttl", 30000 } };
        private readonly List<string> _tags;

        public Queue(ISettings settings)
        {
            _model = settings.Model;
            _exchange = settings.Exchange;
            _model.ExchangeDeclare(exchange: _exchange, type: ExchangeType.Direct, arguments: _properties);
            _tags = new List<string>();
        }

        public void Publish<T>(T message, string queue)
        {
            var jmessage = JsonConvert.SerializeObject(message);
            var body = Encoding.UTF8.GetBytes(jmessage);
            _model.BasicPublish(_exchange, queue, null, body);
        }

        public void Subscribe<T>(Action<T> action, string thisQueue, string readingFromQueue)
        {
            _model.QueueDeclare(thisQueue, durable: true, exclusive: false, autoDelete: false, arguments: null);
            _model.QueueDeclare(readingFromQueue, durable: true, exclusive: false, autoDelete: false, arguments: null);
            _model.QueueBind(thisQueue, _exchange, readingFromQueue);
            var subscriber = new AsyncEventingBasicConsumer(_model);
            subscriber.Received += async (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                T jmessage = JsonConvert.DeserializeObject<T>(message);
                await Task.Factory.StartNew(() => action?.Invoke(jmessage), TaskCreationOptions.RunContinuationsAsynchronously);
            };
            string tag = _model.BasicConsume(queue: thisQueue, autoAck: true, consumer: subscriber);
            _tags.Add(tag);
        }

        public void Unsubscribe()
        {
            _tags.ForEach(f => _model.BasicCancel(f));
            _model.Close();
            _model.Dispose();
        }
    }
}
